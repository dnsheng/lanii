# lanii - Local Anime Information

### Description:

lanii is an anime media center that can scrape information about any anime series or movie as well as the characters and staff for local storage and viewing.

### Requirements:

+ python 3.6+
+ requests
+ urllib3
+ sip
+ PyQt4
+ Python Imaging Library (PIL) 
+ sqlite3
+ BeautifulSoup
+ mpv

( While pip will install most of the above, sip and PyQt4 are not available and must
be installed from somewhere else)

### Installation:

Go to the folder containing setup.py and run the following command:

```
python setup.py install
```

### Previews:

![alt text](images/mainpage.png)

![alt text](images/castpage.png)

![alt text](images/charpage.png)

![alt text](images/actorpage.png)

### TODO:
```
Rewrites:
+ Decide when to include data in dictionary versus calling on demand (ex. series sequels)
+ Store all series/characters/people in memory and only hide display when searching
  (As opposed to the current removal and recreation)
+ Re-structure db_ops.py and db_utils.py (things like the retrieve() function)
+ Redesign relationship between Module and Scraper (more explicit)
+ Make more functions hidden
+ Migration from PyQt4 to PySide2
+ Refactoring and removing more code smells

New features:
+ Multi-processing for related/recommended series
+ Complete the import and removal functionalities
+ Confirmation dialog before any right-click imports (re-import or new import)
+ Better exception handling
+ Hotkeys and shortcuts
+ Import and export database/images for lanii
+ Preview images for series, more images for characters and staff/actors
+ Module selection menu when importing
+ More modules for different websites (how to link IDs between them?)
+ Create unit tests

Fixes:
+ A work around for HTTP 429 errors
+ QSS tweaks for consistent look on different platforms

Deployment:
+ Test installation on different platforms to ensure correct dependencies and such

Non code-related:
+ Create an icon/mascot for lanii
```
