import os

VERSION = "0.0.1"

FILE_TYPES = (".avi", ".mp4", ".mkv", ".mkv.sftp")

RUN_PATH = os.path.dirname(__file__)

USER_HOME = os.path.expanduser("~")
LANII_PATH = os.path.expanduser("~/.local/share/lanii")
ASSET_PATH = os.path.join(RUN_PATH, 'assets')

# Storage paths
IMAGE_PATH = os.path.join(LANII_PATH, "img")
SERIES_IMG_PATH = os.path.join(IMAGE_PATH, "series")
CHAR_IMG_PATH = os.path.join(IMAGE_PATH, "character")
PERSON_IMG_PATH = os.path.join(IMAGE_PATH, "person")

DATABASE_PATH = os.path.join(LANII_PATH, "series.db")

# Asset paths
IMAGE_ASSETS = os.path.join(ASSET_PATH, "images")
NO_IMG_ASSET = os.path.join(IMAGE_ASSETS, "no_img.jpg")
LOADING_GIF = os.path.join(IMAGE_ASSETS, "loading.gif")

QT_ASSETS = os.path.join(ASSET_PATH, "qt")
DEFAULT_QSS = os.path.join(QT_ASSETS, "default.qss")
