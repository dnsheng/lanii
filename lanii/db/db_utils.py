# Functions that add entries to a database

# Add a series to the database
# Requires the database conn and a series object
# Does nothing if series already exists in database currently (should update entry)
def db_write_series(conn, s):
    c = conn.cursor()
    # Check for existence in the database
    s_id = (s['id'],)
    EXISTS = c.execute('SELECT * FROM series WHERE series_id=?', s_id).fetchall()
    # Doesn't exist, make a new entry
    if not EXISTS:
        s_load = (s['id'], s['image'], s['name'], s['synopsis'], s['format'],
		  s['episodes'], s['status'], s['aired'], s['premier'], s['broadcast'],
		  s['source'], s['duration'], s['rating'], s['score'], s['path'])
        c.execute('INSERT INTO series VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', s_load)
    else:
        # Update existing
        s_load = (s['image'], s['name'], s['synopsis'], s['format'], s['episodes'], 
                  s['status'], s['aired'], s['premier'], s['broadcast'], s['source'],
		  s['duration'], s['rating'], s['score'], s['path'], s['id'])
        c.execute('''UPDATE series SET
                     image=?, title=?, synopsis=?, format_type=?, episodes=?,
                     status=?, aired=?, premier=?, broadcast=?, source=?, duration=?,
                     rating=?, score=?, path=?
                     WHERE series_id=?''', s_load)
    conn.commit()

# Add a character to the database
# Requires the database conn and a character object
# Does nothing if character already exists in database currently (should update entry)
def db_write_character(conn, char):
    c = conn.cursor()
    # Check for existence in the database
    c_id = (char['id'],)
    EXISTS = c.execute('SELECT * FROM characters WHERE char_id=?', c_id).fetchall()
    # Doesn't exist, make a new entry
    if not EXISTS:
        c_load = (char['id'], char['image'], char['name'], char['desc'])
        c.execute('INSERT INTO characters VALUES (?,?,?,?)', c_load)
    else:
        # Update existing
        c_load = (char['image'], char['name'], char['desc'], char['id'])
        c.execute('''UPDATE characters SET
                     image=?, name=?, desc=?
                     WHERE char_id=?''', c_load)
    conn.commit()

# Add a person to the database
# Requires the database conn and a person object
# Does nothing if person already exists in database currently (should update entry)
def db_write_person(conn, p):
    c = conn.cursor()
    # Check for existence in the database
    p_id = (p['id'],)
    EXISTS = c.execute('SELECT * FROM people WHERE person_id=?', p_id).fetchall()
    # Doesn't exist, make a new entry
    if not EXISTS:
        p_load = (p['id'], p['image'], p['name'], p['desc'])
        c.execute('INSERT INTO people VALUES (?,?,?,?)', p_load)
    else:
        # Update existing
        p_load = (p['image'], p['name'], p['desc'], p['id'])
        c.execute('''UPDATE people SET
                     image=?, name=?, desc=?
                     WHERE person_id=?''', p_load)
    conn.commit()

# Bridge the characters to the series and their actor
def db_bridge_SC(conn, char, series):
    cursor = conn.cursor()
    # Check for existence in the database
    c_check = (char['id'],)
    c_list = cursor.execute('SELECT * FROM seriesChar WHERE char_id=?', c_check)
    EXISTS = None
    for c in c_list:
        if series['id'] == str(c[0]):
            EXISTS = True
            break
    if not EXISTS:
        sc_load = (series['id'], char['id'], char['person_id'], char['role'])
        cursor.execute('INSERT INTO seriesChar VALUES (?,?,?,?)', sc_load)
        conn.commit()

# Bridge the staff members to the series
def db_bridge_SS(conn, staff, series):
    c = conn.cursor()
    # Check for existence in the database
    p_check = (staff['id'],)
    p_list = c.execute('SELECT * FROM seriesStaff WHERE person_id=?', p_check)
    EXISTS = None
    for p in p_list:
        if series['id'] == p[0]:
            EXISTS = True
            break
    # Add to database if doesn't exist
    if not EXISTS:
        ss_load = (series['id'], staff['id'], staff['role'])
        c.execute('INSERT INTO seriesStaff VALUES (?,?,?)', ss_load)
        conn.commit()

# Add a dictionary's contents to the database, writing to the lookup and bridge table
# Only adds unique entries, ensuring no duplicates
# Takes in the database, series id, series dictionary, and database code
def db_write_entry(conn, sid, sdict, code):
    # Map table codes to tuples for function re-usability
    tables = {
        'g': ('genres', 'gen_id', 'seriesGen'),
        'p': ('producers', 'prod_id', 'seriesProd'),
        'l': ('licensors', 'lic_id', 'seriesLic'),
        's': ('studios', 'stud_id', 'seriesStud')
    }
    c = conn.cursor()

    # Make copy containing only unique entries not already in the lookup table
    cpy = dict(sdict)
    for key in sdict.keys():
        tmp_key = (key,)
        for row in c.execute('SELECT * FROM ' 
                             + tables[code][0] 
                             + ' WHERE ' 
                             + tables[code][1] 
                             + '=?', tmp_key):
            del cpy[str(row[0])]
    # Insert unique entries to the table
    for key, name in cpy.items():
        load = (key, name)
        c.execute('INSERT INTO ' + tables[code][0] + ' VALUES (?,?)', load)
    # Make copy contain only unique entries not already in the bridge table
    cpy = dict(sdict)
    for key in sdict.keys():
        tmp_key = (sid, key)
        for row in c.execute('SELECT * FROM ' 
                             + tables[code][2] 
                             + ' WHERE series_id=? AND '
                             + tables[code][1] + '=?', tmp_key):
            del cpy[str(row[1])]
    # Insert all unique entries to the bridge table
    for key, name in cpy.items():
        load = (sid, key)
        c.execute('INSERT INTO ' + tables[code][2] + ' VALUES (?,?)', load)

    conn.commit()

# Write the related series to the database
# Given the database, series id, dictionary of relatives, and code
# TODO: Possibly also enter the reverse
#       (Ex. If A has a sequel B, then A is also a prequel to B)
def db_write_relative(conn, s_id, rel_list, code):
    table_dict = {'P': 'prequels',
                  'S': 'sequels',
                  'A': 'alternatives',
                  'Y': 'side_stories',
                  'O': 'other',
                  'X': 'adaptations'}

    table = table_dict[code]
    c = conn.cursor()
    s_check = (s_id,)
    EXISTS = None

    for relative in rel_list:
        s_list = c.execute('SELECT * FROM ' + table + ' WHERE series_id=?', s_check)
        for item in s_list:
            # Check if entry already exists in related table
            if relative[0] == item[1]:
                EXISTS = True
                break
        if not EXISTS:
            rel_load = (s_id, relative[0], relative[1], relative[2])
            c.execute('INSERT INTO ' + table + ' VALUES (?,?,?,?)', rel_load)
            EXISTS = None

    conn.commit()

# Write all recommended series for a series to the database
# Given the database, series id, list of recs (each rec an array of (id, title, img, desc))
def db_write_recs(conn, s_id, rec_list):
    c = conn.cursor()
    s_check = (s_id,)
    EXISTS = None

    for rec in rec_list:
        r_list = c.execute('SELECT * FROM recommendations WHERE series_id=?', s_check)
        for item in r_list:
            # Check if entry already exists in recommendation table
            if rec[0] == item[1]:
                EXISTS = True
                break
        if not EXISTS:
            rec_load = (s_id, rec[0], rec[1], rec[2], rec[3])
            c.execute('INSERT INTO recommendations VALUES (?,?,?,?,?)', rec_load)
            EXISTS = None

    conn.commit()
