import sqlite3

import lanii.db.db_utils as db_utils
import lanii.constants as constants

"""
These are functions involving reading, writing, and deleting with the database
"""

def initialize():
    """
    Init the db with lookup tables and bridge tables
    Create a dummy person to reference if characters have no VA
    """
    conn = sqlite3.connect(constants.DATABASE_PATH)
    c = conn.cursor()
    # Create the lookup tables
    c.execute('''CREATE TABLE series
              (series_id integer UNIQUE, image text, title text UNIQUE,
               synopsis text, format_type text, episodes integer, status text, 
               aired text, premier text, broadcast text, source text, duration text,
               rating text, score text, path text)''')
    c.execute('''CREATE TABLE producers
              (prod_id integer UNIQUE, name text UNIQUE)''')
    c.execute('''CREATE TABLE licensors
              (lic_id integer UNIQUE, name text UNIQUE)''')
    c.execute('''CREATE TABLE studios
              (stud_id integer UNIQUE, name text UNIQUE)''')
    c.execute('''CREATE TABLE genres
              (gen_id integer UNIQUE, name text UNIQUE)''')
    c.execute('''CREATE TABLE characters
              (char_id integer UNIQUE, image text, name text, desc text)''')
    c.execute('''CREATE TABLE people
              (person_id integer UNIQUE, image text, name text, desc text)''')
    # Create the bridge tables
    c.execute('''CREATE TABLE seriesProd
              (series_id integer, prod_id integer)''')
    c.execute('''CREATE TABLE seriesLic
              (series_id integer, lic_id integer)''')
    c.execute('''CREATE TABLE seriesStud
              (series_id integer, stud_id integer)''')
    c.execute('''CREATE TABLE seriesGen
              (series_id integer, gen_id integer)''')
    c.execute('''CREATE TABLE seriesChar
              (series_id integer, char_id integer, person_id integer, char_role text)''')
    c.execute('''CREATE TABLE seriesStaff
              (series_id integer, person_id integer, staff_role text)''')
    c.execute('''CREATE TABLE sequels
              (series_id integer, sequel_id integer, image text, title text)''')
    c.execute('''CREATE TABLE prequels
              (series_id integer, prequel_id integer, image text, title text)''')
    c.execute('''CREATE TABLE alternatives
              (series_id integer, alternative_id integer, image text, 
               title text)''')
    c.execute('''CREATE TABLE side_stories
              (series_id integer, side_story_id integer, image text, title text)''')
    c.execute('''CREATE TABLE other
              (series_id integer, other_id integer, image text, title text)''')
    c.execute('''CREATE TABLE recommendations
              (series_id integer, rec_id integer, image text, title text,
               desc text)''')

    # Create a dummy person for unknown voice actors
    p = {'id': 0, 'image': constants.NO_IMG_ASSET, 'name': "Unknown", 'desc': "",
         'role': ""}
    db_utils.db_write_person(conn, p)
            
    conn.commit()

def retrieve():
    """
    Return the db
    """
    conn = sqlite3.connect(constants.DATABASE_PATH)
    return conn

def add_series(s):
    """
    Add entries to the db for a new series
    """
    conn = retrieve()
    db_utils.db_write_series(conn, s)
    db_utils.db_write_entry(conn, s['id'], s['producers'], 'p')
    db_utils.db_write_entry(conn, s['id'], s['licensors'], 'l')
    db_utils.db_write_entry(conn, s['id'], s['studios'], 's')
    db_utils.db_write_entry(conn, s['id'], s['genres'], 'g')
    db_utils.db_write_relative(conn, s['id'], s['prequels'], 'P')
    db_utils.db_write_relative(conn, s['id'], s['sequels'], 'S')
    db_utils.db_write_relative(conn, s['id'], s['sides'], 'Y')
    db_utils.db_write_relative(conn, s['id'], s['alternates'], 'A')
    db_utils.db_write_relative(conn, s['id'], s['others'], 'O')
    db_utils.db_write_recs(conn, s['id'], s['recs'])

def add_character(c, s):
    """
    Add a character to the db
    """
    conn = retrieve()
    db_utils.db_write_character(conn, c)
    db_utils.db_bridge_SC(conn, c, s)

def add_actor(p):
    """
    Add an actor to the db
    """
    conn = retrieve()
    db_utils.db_write_person(conn, p)

def add_staff(p, s):
    """
    Add a staff member to the db
    """
    conn = retrieve()
    db_utils.db_write_person(conn, p)
    db_utils.db_bridge_SS(conn, p, s)

def remove_series(title):
    """
    Remove a series based on the id
    """
    conn = retrieve()
    c = conn.cursor()
    series = series_from_title(title)
    s_id = series['id']
    c.execute('DELETE FROM series WHERE series_id=?', (s_id,))
    c.execute('DELETE FROM seriesProd WHERE series_id=?', (s_id,))
    c.execute('DELETE FROM seriesLic WHERE series_id=?', (s_id,))
    c.execute('DELETE FROM seriesStud WHERE series_id=?', (s_id,))
    c.execute('DELETE FROM seriesGen WHERE series_id=?', (s_id,))
    c.execute('DELETE * FROM sequels WHERE series_id=?', (s_id,))
    c.execute('DELETE * FROM prequels WHERE series_id=?', (s_id,))
    c.execute('DELETE * FROM alternatives WHERE series_id=?', (s_id,))
    c.execute('DELETE * FROM side_stories WHERE series_id=?', (s_id,))
    c.execute('DELETE * FROM other WHERE series_id=?', (s_id,))
    c.execute('DELETE * FROM recommendations WHERE series_id=?', (s_id,))
    # conn.commit()

def remove_character(id_):
    """
    Remove a character from the db by their id
    """
    pass

def remove_person(id_):
    """
    Remove a person from the db by their id
    """
    pass

def change_series_path(series, path):
    """
    Change the path of a series in the database
    """
    conn = retrieve()
    c = conn.cursor()
    c.execute('UPDATE series SET path=? WHERE title=?', (path, series))
    conn.commit()

### OBJECT CREATION FUNCTIONS ###

def _series_from_db(entry):
    """
    Create a Series dict from the entry of a series in the db
    """
    if entry:
        id_ = entry[0]
        series = {'id': id_, 'image': entry[1], 'name': entry[2], 'synopsis': entry[3],
                  'format': entry[4], 'episodes': entry[5], 'status': entry[6],
                  'aired': entry[7], 'premier': entry[8], 'broadcast': entry[9],
                  'source': entry[10], 'duration': entry[11], 'rating': entry[12],
                  'score': entry[13], 'path': entry[14], 'producers': get_producers(id_),
                  'licensors': get_licensors(id_), 'studios': get_studios(id_),
                  'genres': get_genres(id_), 'prequels': get_prequels(id_),
                  'alternates': get_alts(id_), 'sides': get_sides(id_), 
                  'sequels': get_sequels(id_), 'others': get_others(id_),
                  'recs': get_recs(id_)}
    else:
        series = None
    return series

def _char_from_db(entry):
    """
    Create a Character dict from the entry of a character in the db
    """
    if entry:
        id_ = entry[0]
        c = {'id': id_, 'image': entry[1], 'name': entry[2], 'desc': entry[3]}
             #'appearances': get_appearances(id_)}
    else:
        c = None
    return c

def _person_from_db(entry):
    """
    Create a Person dict from the entry of a person in the db
    """
    if entry:
        id_ = entry[0]
        p = {'id': id_, 'image': entry[1], 'name': entry[2], 'desc': entry[3],
             'positions': get_positions(id_), 'roles': get_roles(id_)}
    else:
        p = None
    return p

### INFORMATION RETURN FUNCTIONS ###

def get_all_series():
    """
    Return all series in db
    """
    series_list = []
    c = retrieve().cursor()
    entries = c.execute('SELECT * FROM series ORDER BY title').fetchall()
    for entry in entries:
        s = _series_from_db(entry)
        series_list.append(s)
    return series_list

def get_all_chars():
    """
    Return all characters in db
    """
    char_list = []
    c = retrieve().cursor()
    entries = c.execute('SELECT * FROM characters ORDER BY name').fetchall()
    for entry in entries:
        c = _char_from_db(entry)
        char_list.append(c)
    return char_list

def get_all_people():
    """
    Return all people in db
    """
    people_list = []
    c = retrieve().cursor()
    entries = c.execute('SELECT * FROM people ORDER BY name').fetchall()
    for entry in entries:
        p = _person_from_db(entry)
        people_list.append(p)
    return people_list

def series_from_title(title):
    """
    Return series in db from title
    """
    c = retrieve().cursor()
    entry = c.execute('SELECT * FROM series WHERE title=?', (title,)).fetchone()
    s = _series_from_db(entry)
    return s

def series_from_id(s_id):
    """
    Return series in db from id
    """
    c = retrieve().cursor()
    entry = c.execute('SELECT * FROM series WHERE series_id=?', (s_id,)).fetchone()
    s = _series_from_db(entry)
    return s

def char_from_name(name):
    """
    Return character in db from name
    """
    c = retrieve().cursor()
    entry = c.execute('SELECT * FROM characters WHERE name=?', (name,)).fetchone()
    char = _char_from_db(entry)
    return char

def char_from_id(c_id):
    """
    Return character in db from id
    """
    c = retrieve().cursor()
    entry = c.execute('SELECT * FROM characters WHERE char_id=?', (c_id,)).fetchone()
    char = _char_from_db(entry)
    return char

def person_from_name(name):
    """
    Return person in db from name
    """
    c = retrieve().cursor()
    entry = c.execute('SELECT * FROM people WHERE name=?', (name,)).fetchone()
    p = _person_from_db(entry)
    return p

def person_from_id(p_id):
    """
    Return person in db from id
    """
    c = retrieve().cursor()
    entry = c.execute('SELECT * FROM people WHERE person_id=?', (p_id,)).fetchone()
    p = _person_from_db(entry)
    return p

def actor_from_char_id(c_id):
    """
    Return person in db from a character id (a character they voiced)
    """
    c = retrieve().cursor()
    sc = c.execute('SELECT * FROM seriesChar WHERE char_id=?', (c_id,)).fetchone()
    entry = c.execute('SELECT * FROM people WHERE person_id=?', (sc[2],)).fetchone()
    actor = _person_from_db(entry)
    return actor

def producer_from_id(p_id):
    """
    Return producer in db from id
    """
    c = retrieve().cursor()
    producer = c.execute('SELECT * FROM producers WHERE prod_id=?', (p_id,)).fetchone()
    return producer

def studio_from_id(s_id):
    """
    Return studio in db from id
    """
    c = retrieve().cursor()
    studio = c.execute('SELECT * FROM studios WHERE stud_id=?', (s_id,)).fetchone()
    return studio

def licensor_from_id(l_id):
    """
    Return licensor in db from id
    """
    c = retrieve().cursor()
    licensor = c.execute('SELECT * FROM licensors WHERE lic_id=?', (l_id,)).fetchone()
    return licensor

def genre_from_id(g_id):
    """
    Return genre in db from id
    """
    c = retrieve().cursor()
    genre = c.execute('SELECT * FROM genres WHERE gen_id=?', (g_id,)).fetchone()
    return genre

def cast_from_series(s_id):
    """
    Return all cast (characters, roles, actors) from a series id
    """
    cast = []
    c = retrieve().cursor()
    for item in c.execute('SELECT * FROM seriesChar WHERE series_id=?', (s_id,)):
        char = char_from_id(item[1])
        # Sometimes a character may not have an actor
        if char['id'] != 0:
            actor = actor_from_char_id(char['id'])
            aname = actor['name']
            aimg = actor['image']
        else:
            aname = ""
            aimg = constants.NO_IMG_ASSET
        role = item[3]
        cast.append((char['name'], char['image'], role, aname, aimg))
    return cast

def staff_from_series(s_id):
    """
    Return all staff members and their roles from a series id
    """
    staff = []
    c = retrieve().cursor()
    for item in c.execute('SELECT * FROM seriesStaff WHERE series_id=?', (s_id,)):
        person = person_from_id(item[1])
        role = item[2]
        staff.append((person['name'], role, person['image']))
    return staff

def get_sequels(s_id):
    """
    Return all sequels in db from a series id
    """
    sequels = []
    c = retrieve().cursor()
    for seq in c.execute('SELECT * FROM sequels WHERE series_id=?', (s_id,)):
        title = seq[3]
        img = seq[2]
        sequels.append((title, img))
    return sequels

def get_prequels(s_id):
    """
    Return all prequels in db from a series id
    """
    prequels = []
    c = retrieve().cursor()
    for pre in c.execute('SELECT * FROM prequels WHERE series_id=?', (s_id,)):
        title = pre[3]
        img = pre[2]
        prequels.append((title, img))
    return prequels

def get_alts(s_id):
    """
    Return all alternative series in db from a series id
    """
    alts = []
    c = retrieve().cursor()
    for alt in c.execute('SELECT * FROM alternatives WHERE series_id=?', (s_id,)):
        title = alt[3]
        img = alt[2]
        alts.append((title, img))
    return alts

def get_sides(s_id):
    """
    Return all side stories in db from a series id
    """
    sides = []
    c = retrieve().cursor()
    for side in c.execute('SELECT * FROM side_stories WHERE series_id=?', (s_id,)):
        title = side[3]
        img = side[2]
        sides.append((title, img))
    return sides

def get_others(s_id):
    """
    Return all related series in db from a series id
    """
    others = []
    c = retrieve().cursor()
    for oth in c.execute('SELECT * FROM other WHERE series_id=?', (s_id,)):
        title = oth[3]
        img = oth[2]
        others.append((title, img))
    return others

def get_recs(s_id):
    """
    Return all recommended series in db from a series id
    """
    recs = []
    c = retrieve().cursor()
    for rec in c.execute('SELECT * FROM recommendations WHERE series_id=?', (s_id,)):
        title = rec[3]
        img = rec[2]
        #desc = rec[4]
        recs.append((title, img))
    return recs

def get_producers(s_id):
    """
    Return all production studios in db from a series id
    """
    producers = []
    c = retrieve().cursor()
    for p in c.execute('SELECT * FROM seriesProd WHERE series_id=?', (s_id,)):
        prod = producer_from_id(p[1])
        producers.append(prod)
    return producers

def get_studios(s_id):
    """
    Return all studios for a series from a series id
    """
    studios = []
    c = retrieve().cursor()
    for s in c.execute('SELECT * FROM seriesStud WHERE series_id=?', (s_id,)):
        stud = studio_from_id(s[1])
        studios.append(stud)
    return studios

def get_licensors(s_id):
    """
    Return all licensors for a series from a series id
    """
    licensors = []
    c = retrieve().cursor()
    for l in c.execute('SELECT * FROM seriesLic WHERE series_id=?', (s_id,)):
        lic = licensor_from_id(l[1])
        licensors.append(lic)
    return licensors

def get_genres(s_id):
    """
    Return all genres for a series from a series_id
    """
    genres = []
    c = retrieve().cursor()
    for g in c.execute('SELECT * FROM seriesGen WHERE series_id=?', (s_id,)):
        gen = genre_from_id(g[1])
        genres.append(gen)
    return genres

def get_appearances(id_):
    """
    Get all appearances of a character from a character name
    """
    apps = []
    c = retrieve().cursor()
    for app in c.execute('SELECT * FROM seriesChar WHERE char_id=?', (id_,)):
        s = series_from_id(app[0])
        apps.append(s)
    return apps

def get_roles(p_id):
    """
    Get all acting roles held by a person from a person's id
    """
    roles = []
    c = retrieve().cursor()
    for sc in c.execute('SELECT * FROM seriesChar WHERE person_id=?', (p_id,)):
        roles.append(sc)
    return roles

def get_positions(p_id):
    """
    Return all staff positions held by a person from a person id
    """
    positions = []
    c = retrieve().cursor()
    for p in c.execute('SELECT * FROM seriesStaff WHERE person_id=?', (p_id,)):
        positions.append(p)
    return positions

def search_series(title):
    """
    Return a list of series with similar titles, given a series title (case insensitive)
    """
    matching = []
    c = retrieve().cursor()
    t = ("%" + title + "%",)
    for entry in c.execute('SELECT * FROM series WHERE title LIKE ? COLLATE NOCASE', t):
        s = _series_from_db(entry)
        matching.append(s)
    return matching

def search_chars(name):
    """
    Return a list of characters with similar names, given a char name (case insensitive)
    """
    matching = []
    c = retrieve().cursor()
    n = ("%" + name + "%",)
    for entry in c.execute('SELECT * FROM characters WHERE name LIKE ? COLLATE NOCASE', n):
        char = _char_from_db(entry)
        matching.append(char)
    return matching

def search_people(name):
    """
    Return a list of people with similar names, given a person's name (case insensitive)
    """
    matching = []
    c = retrieve().cursor()
    n = ("%" + name + "%",)
    for entry in c.execute('SELECT * FROM people WHERE name LIKE ? COLLATE NOCASE', n):
        p = _person_from_db(entry)
        matching.append(p)
    return matching

def search_genres(string):
    """
    Return a list of series with the same genre, given a genre name (case insensitive)
    """
    matching = []
    c = retrieve().cursor()
    n = ("%" + string + "%",)
    for g in c.execute('SELECT * FROM genres WHERE name LIKE ? COLLATE NOCASE', n):
        for b in c.execute('SELECT * FROM seriesGen WHERE gen_id=?', (g[0],)):
            for entry in c.execute('SELECT * FROM series WHERE series_id=?', (b[0],)):
                s = _series_from_db(entry)
                matching.append(s)
    return matching

def search_studios(string):
    """
    Return a list of series with the same studio, given a studio name (case insensitive)
    """
    matching = []
    c = retrieve().cursor()
    n = ("%" + string + "%",)
    for g in c.execute('SELECT * FROM studios WHERE name LIKE ? COLLATE NOCASE', n):
        for b in c.execute('SELECT * FROM seriesStud WHERE stud_id=?', (g[0],)):
            for entry in c.execute('SELECT * FROM series WHERE series_id=?', (b[0],)):
                s = _series_from_db(entry)
                matching.append(s)
    return matching

def search_producers(string):
    """
    Return a list of series with the same producer, given a prod name (case insensitive)
    """
    matching = []
    c = retrieve().cursor()
    n = ("%" + string + "%",)
    for g in c.execute('SELECT * FROM producers WHERE name LIKE ? COLLATE NOCASE', n):
        for b in c.execute('SELECT * FROM seriesProd WHERE prod_id=?', (g[0],)):
            for entry in c.execute('SELECT * FROM series WHERE series_id=?', (b[0],)):
                s = _series_from_db(entry)
                matching.append(s)
    return matching

def search_licensors(string):
    """
    Return a list of series with the same licensor, given a lic. name (case insensitive)
    """
    matching = []
    c = retrieve().cursor()
    n = ("%" + string + "%",)
    for g in c.execute('SELECT * FROM licensors WHERE name LIKE ? COLLATE NOCASE', n):
        for b in c.execute('SELECT * FROM seriesLic WHERE lic_id=?', (g[0],)):
            for entry in c.execute('SELECT * FROM series WHERE series_id=?', (b[0],)):
                s = _series_from_db(entry)
                matching.append(s)
    return matching
