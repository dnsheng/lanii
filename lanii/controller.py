import os

import lanii.tools as tools
import lanii.db.db_ops as db
import lanii.format as sformat
import lanii.constants as constants
import lanii.db.db_utils as db_utils

from lanii.scrape.scraper import Scrape
from lanii.scrape.mal_module import MALModule

def import_series(folder):
    """
    Given a folder name, scrape and add the series to the database
    """
    # Check folder for validity
    if tools.valid_folder(folder):
        series_name = folder.rsplit('/', 1)[1]
        _import_series(series_name, folder)
    else:
        # Invalid folder, assume to be just name of series
        # Create a fake folder as placeholder, user can change to actual later
        fake_folder = os.path.join(constants.LANII_PATH, folder)
        _import_series(folder, fake_folder)

def _import_series(series_name, folder):
    """
    Scrape info on a series and add to the database
    """
    # Init module and scraper, scrape
    module = MALModule()
    scraper = Scrape(module)
    scraps = scraper.scrape_all(series_name, folder)
    # Scrape series
    if scraps:
        # Scraping successful, commit to database
        db.add_series(scraps['series'])
        for c in scraps['characters']:
            db.add_character(c, scraps['series'])
        for a in scraps['actors']:
            db.add_actor(a)
        for s in scraps['staff']:
            db.add_staff(s, scraps['series'])
        # Delete objects for thread exit
        del scraper
        del module
    else:
        # Scraping was a failure (not found or connection error)
        del scraper
        del module

def import_character(name):
    """
    Given a name, scrape and add the character to the database
    """
    # Get the name and id of character
    char_name = sformat.remove_quotes(name)
    char_id = db.char_from_name(char_name)[0]
    # Init module and scraper
    module = MALModule()
    scraper = Scrape(module)
    # Scrape the character
    char = scraper.scrape_char_nourl(char_name, char_id)
    # Add to database
    conn = db.retrieve()
    db_utils.db_write_character(conn, char)
    # Clean up
    del scraper
    del module

def import_person(name):
    """
    Given a person's name, scrape and add the person to the database
    """
    # Get id of person
    person_id = db.person_from_name(name)[0]
    # Init module and scraper
    module = MALModule()
    scraper = Scrape(module)
    # Scrape the person
    person = scraper.scrape_person_nourl(name, person_id)
    # Add to database
    conn = db.retrieve()
    db_utils.db_write_person(person)
    # Clean up
    del scraper
    del module

def import_all(series):
    """
    What is this?
    """
    pass

def remove_series(series):
    """
    Given a series's name, remove it from the database
    """
    pass

def remove_character(name):
    """
    Given a character's name, remove them from the database
    """
    # Get the id of the character
    char_id = db.char_from_name(name)[0]
    # Remove character from the database
    #db.remove_character(char_id)
    pass

def remove_person(name):
    """
    Given a person's name, remove them from the database
    """
    # Get id of person
    person_id = db.person_from_name(name)[0]
    # Remove person from the database
    #db.remove_person(person_id)
    pass
