import re

import lanii.db.db_ops as db
import lanii.constants as constants

### LOCAL CONSTANTS ###

tab = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
header = "<html><head/><body>"
tail = "</body></html>"
span_s = "<span style=\"font-weight:600; color: rgb(220,220,220);\">"
span_f = "</span><br>"
p_s = "<p>"
p_f = "</p>"

### FORMAT MISC STRINGS IN LANII ###

def format_subtitle(series):
    """
    The subtitle is displayed differently depending on if the current series is a
    movie or television show.
    """
    subtitle_text = series['duration']
    if series['format'] == "Movie":
        subtitle_text += " runtime"
    else:
        subtitle_text = str(series['episodes']) + " episodes, " + subtitle_text
    return subtitle_text

def clean_name(name):
    """
    For a character name, quotes and their contents are removed.
    If a name is too long, only the first and last name is used.
    This is done in order for the text to fit the list item and title 
    """
    # Remove double quotes and their contents
    new_name = remove_quotes(name)
    # If it is a long name, get only the first and last name
    if len(new_name) > 25:
        first, *middle, last = new_name.split()
        new_name = first + " " + last
    return new_name

def remove_quotes(name):
    """
    Quotes and their contents are removed from a string (usually a name)
    """
    new_name = re.sub(r'[ ]?\".+?\"', '', name).strip()
    return new_name

def clean_episode(title):
    """
    The name of a video file (episode) is formatted for display.
    """
    # Remove file extensions
    for ext in constants.FILE_TYPES:
        title = title.rsplit(ext, 1)[0]
    # Remove parenthesis
    new_title = re.sub(r'[ ]?\(.*\)', '', title)
    # Replace EP## with Episode ##
    new_title = re.sub(r'EP([0-9]{2})', 'Episode \\1', new_title)
    # Replace periods with space
    new_title = re.sub(r'\.', ' ', new_title)
    # Remove hash
    new_title = re.sub(r'\[[0-9A-Z]{8}\]', '', new_title)
    # Replace underline with space
    new_title = re.sub(r'_', ' ', new_title)
    # Remove any [] boxes not at with sub name and not resoultion
    new_title = re.sub(r'(.+)\[.*[^p]\]', '\\1', new_title)
    new_title = re.sub(r'(.+)\[.*[^p]\]', '\\1', new_title)

    return new_title

def clean_role(role):
    """
    The roles of a staff on a series has spaces replaced with newline chars.
    This makes the string more compact for display on an entry widget.
    """
    new_role = re.sub(r', ', '\n', role)
    return new_role

def format_entry_title(title):
    """
    Sometimes the title for a related or recommended series is too long.
    This caps the size to 40 chars and replaces the rest with ellipses
    """
    if len(title) > 40:
        title = title[:40] + "..."
    return title

def format_actor_name(name):
    """
    The first and last name of an actor are broken to two different lines
    This is uses when displaying the actor of a character in the character page
    """
    new_name = re.sub(r', ', '\n', name)
    return new_name

def revert_actor_name(name):
    """
    Revert the formatting done by format_actor_name()
    This is necessary to get the original string, which is used for searching the database
    """
    new_name = re.sub(r'\n', ', ', name)
    return new_name

### HTML AND RICH TEXT ###

def format_line(bold, line):
    """
    This formats a single line in the description
    Bold is the string that must be in bold text
    Line is the remainder text
    """
    if line:
        formatted = p_s + span_s + bold + span_f + tab + line + p_f
        return formatted

def format_dialog(msg):
    """
    This formats the text/message found in popup dialog boxes
    """
    body = "<p align=\"center\">"
    body += re.sub("\n", "<br>", msg)

    new_msg = header + body + p_f + tail

    return new_msg

# A format_line() for multiple items (ex. series, genres, etc.)
def format_series_list(bold, item_list):
    """
    This performs format_lin() for a list of multiple items (series, genres. etc.)
    The contents of the item list are combined to one line/string separated by commas
    and spaces
    """
    line = ""
    for item in item_list:
        line += item[1] + ", "
    line = line[:-2]
    final = format_line(bold, line)
    return final

def format_series_info(series):
    """
    The description and all scraped information of a series is formatted to HTML
    """

    info = header

    title = format_line("Title: ", series['name'])
    if title:
        info += title

    s_type = format_line("Type: ", series['format'])
    if s_type:
        info += s_type 

    status = format_line("Status: ", series['status'])
    if status:
        info += status

    aired = format_line("Aired: ", series['aired'])
    if aired:
        info += aired

    premier = format_line("Premiered: ", series['premier'])
    if premier:
        info += premier

    broadcast = format_line("Broadcasted: ", series['broadcast'])
    if broadcast:
        info += broadcast

    source = format_line("Source Material: ", series['source'])
    if source:
        info += source

    rating = format_line("Rating: ", series['rating'])
    if rating:
        info += rating

    prod_list = db.get_producers(series['id'])
    if prod_list:
        producers = format_series_list("Producers: ", prod_list)
        info += producers
    
    stud_list = db.get_studios(series['id'])
    if stud_list:
        studios = format_series_list("Studios: ", stud_list)
        info += studios
    
    lic_list = db.get_licensors(series['id'])
    if lic_list:
        licensors = format_series_list("Licensors: ", lic_list)
        info += licensors
    
    gen_list = db.get_genres(series['id'])
    if gen_list:
        genres = format_series_list("Genres: ", gen_list)
        info += genres

    desc = format_line("Description: ", series['synopsis'])
    if desc:
        info += desc.replace('\n\r\n', '<br>' + tab)

    info += tail

    return info

def format_char_info(char):
    """
    The description and all scraped information of a character is formatted to HTML
    """
    info = header

    name = format_line("Name: ", char['name'])
    if name:
        info += name

    if char['desc']:
        # Format any XXXX:YYYY with "XXXX" in the span
        desc = re.sub(r'(.*:)(.*)\n', 
                      p_s + span_s + '\\1' + span_f + tab + '\\2' + p_f, 
                      char['desc'])
        if "</p>" in desc:
            # Split the "XXXX:YYYY"(head) from the main description text body
            head = desc.rsplit('</p>', 1)[0] + p_f
            body = desc.rsplit('</p>', 1)[1]
            # Add the bold "Description:" before the text body
            body = format_line("Description: ", body)
            info += head + body
        else:
            head = format_line("Description: ", desc)
            info += head

    info += tail

    return info
