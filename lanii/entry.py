import os
import sys

from PyQt4 import QtGui

import lanii.db.db_ops as db
import lanii.constants as constants

from lanii.qt.lanii import QtLanii

def lanii_init():
    """
    Check the folder structure for storing data and the database itself.
    Anything not found is created.
    """
    # Create folder structure
    if not os.path.isdir(constants.LANII_PATH):
        os.mkdir(constants.LANII_PATH)
    if not os.path.isdir(constants.IMAGE_PATH):
        os.mkdir(constants.IMAGE_PATH)
    if not os.path.isdir(constants.SERIES_IMG_PATH):
        os.mkdir(constants.SERIES_IMG_PATH)
    if not os.path.isdir(constants.CHAR_IMG_PATH):
        os.mkdir(constants.CHAR_IMG_PATH)
    if not os.path.isdir(constants.PERSON_IMG_PATH):
        os.mkdir(constants.PERSON_IMG_PATH)
    # Create database
    if not os.path.isfile(constants.DATABASE_PATH):
        db.initialize()

def lanii_qt():
    """
    This is one function to start the program
    Start lanii with Qt as the GUI
    """
    # Check directory structure and db
    lanii_init()
    # Create and display the main window, exiting when finished
    app = QtGui.QApplication(sys.argv)
    main_window = QtLanii()
    main_window.show()
    x = app.exec_()
    sys.exit(x)
