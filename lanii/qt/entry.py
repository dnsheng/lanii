from PyQt4 import QtCore, QtGui

class QEntry(QtGui.QWidget):
    """
    Inherits from QWidget and meant for entries in the scroll area.
    Specifically, this is for allowing a mousePressEvent, and signalling when it occurs.
    When pressed, information is returned in the clicked() signal.
    Information must be set before hand with the setInfo() function.
    What info is set and how info is interpreted is the responsibility of the creator.
    It also changes the cursor when hovered to show the user it is clickable.
    """
    clicked = QtCore.pyqtSignal(QtGui.QLabel)
    right_clicked = QtCore.pyqtSignal(QtGui.QLabel)

    def __init__(self, parent=None):
        super(QEntry, self).__init__(parent)
        self.setMouseTracking(True)

    def enterEvent(self, event):
        self.setCursor(QtCore.Qt.PointingHandCursor)

    def leaveEvent(self, event):
        self.setCursor(QtCore.Qt.ArrowCursor)
    
    # A custom function, not sure if the __init__ can be safely changed
    def setInfo(self, info):
        self.info = info 
    
    # Detect only the left mouse clicks and emit signal
    def mousePressEvent(self, event):
        if event.button() == QtCore.Qt.LeftButton:
            self.clicked.emit(self.info)
        elif event.button() == QtCore.Qt.RightButton:
            self.right_clicked.emit(self.info)
    
    # Needed for the qss to work on custom widgets
    def paintEvent(self, pe):
        opt = QtGui.QStyleOption()
        opt.init(self)
        p = QtGui.QPainter(self)
        s = self.style()
        s.drawPrimitive(QtGui.QStyle.PE_Widget, opt, p, self)
