# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'finalized_noprev.ui'
#
# Created by: PyQt4 UI code generator 4.12.1
#
# WARNING! All changes made in this file will be lost!

import sip
from PIL import Image
from PIL.ImageQt import ImageQt
from PyQt4 import QtCore, QtGui

import lanii.tools as tools
import lanii.db.db_ops as db
import lanii.format as sformat
import lanii.constants as constants
from lanii.qt.entry import QEntry
from lanii.qt.confirm import QConfirm
from lanii.qt.epushbutton import QEPushButton
from lanii.qt.threads import QImportSeries, QImportChar, QImportPerson

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

# Main Qt window and thread for lanii
class QtLanii(QtGui.QMainWindow):
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        self.setupUi(self)
        # Variables for simplifying image building arguments
        self.SMALL_IMG_SIZE = (96, 150)
        self.FULL_IMG_SIZE = (None, None)
        self.LOADING_GIF_SIZE = (16, 16)
        # State variables to remember text of widgets when switching between pages
        self.series_path = ""
        self.load_char_state = ""
        self.load_series_state = ""
        self.load_person_state = ""
        # Remember the currenly displayed series
        self.current_series = None
        # Remmeber the series for the currently displayed cast entries page
        self.cast_entry_series = None
        # Variables to map display strings to database strings
        self.char_names = {}
        self.ep_names = {}
        # Import thread, ensure only one exists
        self.import_thread = None
        self.thread_exists = False

        # Add the loading gif to the status bar here
        movie = QtGui.QMovie(constants.LOADING_GIF)
        self.loading_gif.setMovie(movie)
        self.statusbar.addPermanentWidget(self.loading_gif)
        
        # Populate the list widgets and load the first series
        self.populate()

    # Create main widgets for GUI
    def setupUi(self, lanii_main):
        lanii_main.resize(1336, 738)
        self.centralwidget = QtGui.QWidget(lanii_main)
        self.horizontalLayout_9 = QtGui.QHBoxLayout(self.centralwidget)
        self.tabs = QtGui.QTabWidget(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, 
                                       QtGui.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.tabs.sizePolicy().hasHeightForWidth())
        self.tabs.setSizePolicy(sizePolicy)
        self.tabs.setMinimumSize(QtCore.QSize(250, 0))
        self.series_tab = QtGui.QWidget()
        self.horizontalLayout_5 = QtGui.QHBoxLayout(self.series_tab)
        self.horizontalLayout_5.setMargin(0)
        self.series_list = QtGui.QListWidget(self.series_tab)
        self.series_list.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.horizontalLayout_5.addWidget(self.series_list)
        self.tabs.addTab(self.series_tab, _fromUtf8(""))
        self.char_tab = QtGui.QWidget()
        self.char_tab.setProperty("mid", "true")
        self.horizontalLayout_4 = QtGui.QHBoxLayout(self.char_tab)
        self.horizontalLayout_4.setMargin(0)
        self.char_list = QtGui.QListWidget(self.char_tab)
        self.char_list.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.horizontalLayout_4.addWidget(self.char_list)
        self.tabs.addTab(self.char_tab, _fromUtf8(""))
        self.people_tab = QtGui.QWidget()
        self.horizontalLayout_3 = QtGui.QHBoxLayout(self.people_tab)
        self.horizontalLayout_3.setMargin(0)
        self.people_list = QtGui.QListWidget(self.people_tab)
        self.people_list.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.horizontalLayout_3.addWidget(self.people_list)
        self.tabs.addTab(self.people_tab, _fromUtf8(""))
        self.horizontalLayout_9.addWidget(self.tabs)
        self.main_layout = QtGui.QVBoxLayout()
        self.main_layout.setContentsMargins(-1, -1, -1, 0)
        self.search_layout = QtGui.QHBoxLayout()
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, 
                                       QtGui.QSizePolicy.Minimum)
        self.search_layout.addItem(spacerItem)
        self.search_filter = QtGui.QComboBox(self.centralwidget)
        self.search_filter.addItem(_fromUtf8(""))
        self.search_filter.setItemText(0, _fromUtf8(""))
        self.search_filter.addItem(_fromUtf8(""))
        self.search_filter.addItem(_fromUtf8(""))
        self.search_filter.addItem(_fromUtf8(""))
        self.search_filter.addItem(_fromUtf8(""))
        self.search_filter.addItem(_fromUtf8(""))
        self.search_filter.addItem(_fromUtf8(""))
        self.search_filter.addItem(_fromUtf8(""))
        self.search_layout.addWidget(self.search_filter)
        self.search_box = QtGui.QLineEdit(self.centralwidget)
        self.search_box.setAlignment(QtCore.Qt.AlignRight)
        self.search_layout.addWidget(self.search_box)
        self.main_layout.addLayout(self.search_layout)
        self.title_label = QtGui.QLabel(self.centralwidget)
        self.title_label.setIndent(10)
        self.title_label.setProperty("title", "true")
        self.main_layout.addWidget(self.title_label)
        self.nav_layout = QtGui.QHBoxLayout()
        self.subtitle = QtGui.QLabel(self.centralwidget)
        self.subtitle.setIndent(10)
        self.subtitle.setProperty("subtitle", "true")
        self.nav_layout.addWidget(self.subtitle)
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, 
                                        QtGui.QSizePolicy.Minimum)
        self.nav_layout.addItem(spacerItem1)
        self.button_left = QEPushButton(self.centralwidget)
        self.button_left.setMinimumSize(QtCore.QSize(20, 20))
        self.button_left.setMaximumSize(QtCore.QSize(20, 20))
        self.button_left.setProperty("nav", "true")
        self.nav_layout.addWidget(self.button_left)
        self.button_right = QEPushButton(self.centralwidget)
        self.button_right.setMinimumSize(QtCore.QSize(20, 20))
        self.button_right.setMaximumSize(QtCore.QSize(20, 20))
        self.button_right.setProperty("nav", "true")
        self.nav_layout.addWidget(self.button_right)
        self.main_layout.addLayout(self.nav_layout)
        self.stackedWidget = QtGui.QStackedWidget(self.centralwidget)
        self.series_eps = QtGui.QWidget()
        self.horizontalLayout_8 = QtGui.QHBoxLayout(self.series_eps)
        self.horizontalLayout_8.setMargin(0)
        self.horizontalLayout_7 = QtGui.QHBoxLayout()
        self.horizontalLayout_7.setSpacing(6)
        self.verticalLayout_5 = QtGui.QVBoxLayout()
        self.score_label = QtGui.QLabel(self.series_eps)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, 
                                       QtGui.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.score_label.sizePolicy().hasHeightForWidth())
        self.score_label.setSizePolicy(sizePolicy)
        self.score_label.setMaximumSize(QtCore.QSize(16777215, 50))
        self.score_label.setProperty("score", "true")
        self.verticalLayout_5.addWidget(self.score_label, 0, QtCore.Qt.AlignHCenter)
        self.series_eps_img = QtGui.QLabel(self.series_eps)
        self.series_eps_img.setMinimumSize(QtCore.QSize(225, 350))
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, 
                                       QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.series_eps_img.sizePolicy().hasHeightForWidth())
        self.series_eps_img.setSizePolicy(sizePolicy)
        self.verticalLayout_5.addWidget(self.series_eps_img, 0, QtCore.Qt.AlignTop)
        self.horizontalLayout_7.addLayout(self.verticalLayout_5)
        self.verticalLayout_6 = QtGui.QVBoxLayout()
        self.series_eps_tabs = QtGui.QTabWidget(self.series_eps)
        self.series_eps_tabs.setMinimumSize(QtCore.QSize(650, 0))
        sizePol = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, 
                                    QtGui.QSizePolicy.Expanding)
        sizePol.setHorizontalStretch(0)
        sizePol.setVerticalStretch(0)
        sizePol.setHeightForWidth(self.series_eps_tabs.sizePolicy().hasHeightForWidth())
        self.series_eps_tabs.setSizePolicy(sizePol)
        self.episode_tab = QtGui.QWidget()
        self.horizontalLayout = QtGui.QHBoxLayout(self.episode_tab)
        self.horizontalLayout.setMargin(0)
        self.horizontalLayout.setSpacing(0)
        self.episode_list = QtGui.QListWidget(self.episode_tab)
        self.horizontalLayout.addWidget(self.episode_list)
        self.series_eps_tabs.addTab(self.episode_tab, _fromUtf8(""))
        self.related_tab = QtGui.QWidget()
        self.verticalLayout_17 = QtGui.QVBoxLayout(self.related_tab)
        self.verticalLayout_17.setMargin(0)
        self.related_area = QtGui.QScrollArea(self.related_tab)
        self.related_area.setWidgetResizable(True)
        self.related_area.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.scrollAreaWidgetContents_5 = QtGui.QWidget()
        self.scrollAreaWidgetContents_5.setGeometry(QtCore.QRect(0, 0, 597, 505))
        self.scrollAreaWidgetContents_5.setProperty("scrollarea", "true")
        self.related_layout = QtGui.QVBoxLayout(self.scrollAreaWidgetContents_5)
        self.related_layout.setContentsMargins(0, 0, 0, 0)
        self.related_layout.setMargin(0)
        self.related_area.setWidget(self.scrollAreaWidgetContents_5)
        self.verticalLayout_17.addWidget(self.related_area)
        self.series_eps_tabs.addTab(self.related_tab, _fromUtf8(""))
        self.tab = QtGui.QWidget()
        self.horizontalLayout_18 = QtGui.QHBoxLayout(self.tab)
        self.horizontalLayout_18.setMargin(0)
        self.rec_area = QtGui.QScrollArea(self.tab)
        self.rec_area.setWidgetResizable(True)
        self.rec_area.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.scrollAreaWidgetContents_6 = QtGui.QWidget()
        self.scrollAreaWidgetContents_6.setGeometry(QtCore.QRect(0, 0, 597, 505))
        self.scrollAreaWidgetContents_6.setProperty("scrollarea", "true")
        self.rec_layout = QtGui.QVBoxLayout(self.scrollAreaWidgetContents_6)
        self.rec_layout.setContentsMargins(0, 0, 10, 0)
        self.rec_area.setWidget(self.scrollAreaWidgetContents_6)
        self.horizontalLayout_18.addWidget(self.rec_area)
        self.series_eps_tabs.addTab(self.tab, _fromUtf8(""))
        self.verticalLayout_6.addWidget(self.series_eps_tabs)
        self.play_button = QEPushButton(self.series_eps)
        self.play_button.setProperty("play", "true")
        self.verticalLayout_6.addWidget(self.play_button)
        self.horizontalLayout_7.addLayout(self.verticalLayout_6)
        self.horizontalLayout_8.addLayout(self.horizontalLayout_7)
        self.stackedWidget.addWidget(self.series_eps)
        self.series_info = QtGui.QWidget()
        self.horizontalLayout_10 = QtGui.QHBoxLayout(self.series_info)
        self.horizontalLayout_10.setMargin(0)
        self.horizontalLayout_10.setSpacing(6)
        self.verticalLayout_14 = QtGui.QVBoxLayout()
        self.score_label_info = QtGui.QLabel(self.series_info)
        sizePol = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, 
                                    QtGui.QSizePolicy.Minimum)
        sizePol.setHorizontalStretch(0)
        sizePol.setVerticalStretch(0)
        sizePol.setHeightForWidth(self.score_label_info.sizePolicy().hasHeightForWidth())
        self.score_label_info.setSizePolicy(sizePol)
        self.score_label_info.setMaximumSize(QtCore.QSize(16777215, 50))
        self.score_label_info.setProperty("score", "true")
        self.verticalLayout_14.addWidget(self.score_label_info, 0, QtCore.Qt.AlignHCenter)
        self.series_info_img = QtGui.QLabel(self.series_info)
        self.series_info_img.setMinimumSize(QtCore.QSize(225, 350))
        self.verticalLayout_14.addWidget(self.series_info_img, 0, QtCore.Qt.AlignTop)
        self.horizontalLayout_10.addLayout(self.verticalLayout_14)
        self.series_info_scroll = QtGui.QScrollArea(self.series_info)
        self.series_info_scroll.setWidgetResizable(True)
        self.scrollAreaWidgetContents = QtGui.QWidget()
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 594, 617))
        self.scrollAreaWidgetContents.setProperty("scrollarea", "true")
        self.verticalLayout = QtGui.QVBoxLayout(self.scrollAreaWidgetContents)
        self.verticalLayout.setMargin(0)
        self.series_info_text = QtGui.QLabel(self.scrollAreaWidgetContents)
        self.series_info_text.setWordWrap(True)
        self.series_info_text.setProperty("desc", "true")
        self.series_info_text.setMargin(10)
        self.verticalLayout.addWidget(self.series_info_text, 0, QtCore.Qt.AlignTop)
        self.series_info_scroll.setWidget(self.scrollAreaWidgetContents)
        self.horizontalLayout_10.addWidget(self.series_info_scroll)
        self.stackedWidget.addWidget(self.series_info)
        self.series_cast = QtGui.QWidget()
        self.horizontalLayout_11 = QtGui.QHBoxLayout(self.series_cast)
        self.horizontalLayout_11.setMargin(0)
        self.verticalLayout_10 = QtGui.QVBoxLayout()
        self.char_scroll = QtGui.QScrollArea(self.series_cast)
        self.char_scroll.setWidgetResizable(True)
        self.char_scroll.setMinimumSize(QtCore.QSize(300, 100))
        self.scrollAreaWidgetContents_2 = QtGui.QWidget()
        self.scrollAreaWidgetContents_2.setGeometry(QtCore.QRect(0, 0, 413, 575))
        self.scrollAreaWidgetContents_2.setProperty("scrollarea", "true")
        self.char_layout = QtGui.QVBoxLayout(self.scrollAreaWidgetContents_2)
        self.char_layout.setContentsMargins(0, 0, 0, 0)
        self.char_scroll_title = QtGui.QLabel(self.scrollAreaWidgetContents_2)
        self.char_scroll_title.setMaximumSize(QtCore.QSize(16777215, 40))
        self.char_scroll_title.setProperty("scroll", "true")
        self.verticalLayout_10.addWidget(self.char_scroll_title)
        self.char_scroll.setWidget(self.scrollAreaWidgetContents_2)
        self.verticalLayout_10.addWidget(self.char_scroll)
        self.horizontalLayout_11.addLayout(self.verticalLayout_10)
        self.verticalLayout_11 = QtGui.QVBoxLayout()
        self.staff_scroll = QtGui.QScrollArea(self.series_cast)
        self.staff_scroll.setWidgetResizable(True)
        self.staff_scroll.setMinimumSize(QtCore.QSize(300, 100))
        self.scrollAreaWidgetContents_3 = QtGui.QWidget()
        self.scrollAreaWidgetContents_3.setGeometry(QtCore.QRect(0, 0, 413, 575))
        self.scrollAreaWidgetContents_3.setProperty("scrollarea", "true")
        self.staff_layout = QtGui.QVBoxLayout(self.scrollAreaWidgetContents_3)
        self.staff_layout.setContentsMargins(0, 0, 0, 0)
        self.staff_scroll_title = QtGui.QLabel(self.scrollAreaWidgetContents_3)
        self.staff_scroll_title.setMaximumSize(QtCore.QSize(16777215, 40))
        self.staff_scroll_title.setProperty("scroll", "true")
        self.verticalLayout_11.addWidget(self.staff_scroll_title)
        self.staff_scroll.setWidget(self.scrollAreaWidgetContents_3)
        self.verticalLayout_11.addWidget(self.staff_scroll)
        self.horizontalLayout_11.addLayout(self.verticalLayout_11)
        self.stackedWidget.addWidget(self.series_cast)
        self.char_info = QtGui.QWidget()
        self.horizontalLayout_2 = QtGui.QHBoxLayout(self.char_info)
        self.horizontalLayout_2.setMargin(0)
        self.verticalLayout_12 = QtGui.QVBoxLayout()
        self.char_img = QtGui.QLabel(self.char_info)
        self.char_img.setMinimumSize(QtCore.QSize(225, 350))
        self.char_img.setMaximumSize(QtCore.QSize(16777215, 350))
        self.char_img.setAlignment(QtCore.Qt.AlignCenter)
        self.verticalLayout_12.addWidget(self.char_img)
        self.char_actor_layout = QtGui.QHBoxLayout()
        self.char_actor_img = QEPushButton(self.char_info)
        self.char_actor_img.setMaximumSize(QtCore.QSize(96, 150))
        self.char_actor_layout.addWidget(self.char_actor_img)
        self.char_actor_name = QtGui.QLabel(self.char_info)
        self.char_actor_name.setAlignment(QtCore.Qt.AlignCenter)
        self.char_actor_name.setProperty("actor", "true")
        self.char_actor_layout.addWidget(self.char_actor_name)
        self.verticalLayout_12.addLayout(self.char_actor_layout)
        self.horizontalLayout_2.addLayout(self.verticalLayout_12)
        self.char_tabs = QtGui.QTabWidget(self.char_info)
        self.char_tabs.setTabShape(QtGui.QTabWidget.Rounded)
        self.char_tabs.setIconSize(QtCore.QSize(16, 16))
        self.char_desc_tab = QtGui.QWidget()
        self.horizontalLayout_12 = QtGui.QVBoxLayout(self.char_desc_tab)
        self.horizontalLayout_12.setMargin(0)
        self.char_desc_scroll = QtGui.QScrollArea(self.char_desc_tab)
        self.char_desc_scroll.setWidgetResizable(True)
        self.scrollAreaWidgetContents_8 = QtGui.QWidget()
        self.scrollAreaWidgetContents_8.setGeometry(QtCore.QRect(0, 0, 594, 617))
        self.scrollAreaWidgetContents_8.setProperty("scrolldesc", "true")
        self.verticalLayout_15 = QtGui.QVBoxLayout(self.scrollAreaWidgetContents_8)
        self.verticalLayout_15.setMargin(0)
        self.char_desc = QtGui.QLabel(self.scrollAreaWidgetContents_8)
        self.char_desc.setWordWrap(True)
        self.char_desc.setProperty("desc", "true")
        self.char_desc.setMargin(10)
        self.verticalLayout_15.addWidget(self.char_desc, 0, QtCore.Qt.AlignTop)
        self.char_desc_scroll.setWidget(self.scrollAreaWidgetContents_8)
        self.horizontalLayout_12.addWidget(self.char_desc_scroll)
        self.char_tabs.addTab(self.char_desc_tab, _fromUtf8(""))
        self.char_apps = QtGui.QWidget()
        self.verticalLayout_13 = QtGui.QVBoxLayout(self.char_apps)
        self.verticalLayout_13.setMargin(0)
        self.app_list = QtGui.QListWidget(self.char_apps)
        self.verticalLayout_13.addWidget(self.app_list)
        self.char_tabs.addTab(self.char_apps, _fromUtf8(""))
        self.horizontalLayout_2.addWidget(self.char_tabs)
        self.stackedWidget.addWidget(self.char_info)
        self.person_info = QtGui.QWidget()
        self.horizontalLayout_14 = QtGui.QHBoxLayout(self.person_info)
        self.horizontalLayout_14.setMargin(0)
        self.person_img = QtGui.QLabel(self.person_info)
        self.person_img.setMinimumSize(QtCore.QSize(225, 350))
        self.person_img.setMaximumSize(QtCore.QSize(16777215, 350))
        self.person_img.setProperty("person-img", "true")
        self.person_img.setAlignment(QtCore.Qt.AlignCenter)
        self.horizontalLayout_14.addWidget(self.person_img)
        self.person_tabs = QtGui.QTabWidget(self.person_info)
        self.person_desc_tab = QtGui.QWidget()
        self.horizontalLayout_13 = QtGui.QVBoxLayout(self.person_desc_tab)
        self.horizontalLayout_13.setMargin(0)
        self.person_desc_scroll = QtGui.QScrollArea(self.person_desc_tab)
        self.person_desc_scroll.setWidgetResizable(True)
        self.scrollAreaWidgetContents_9 = QtGui.QWidget()
        self.scrollAreaWidgetContents_9.setGeometry(QtCore.QRect(0, -102, 568, 626))
        self.scrollAreaWidgetContents_9.setProperty("scrolldesc", "true")
        self.horizontalLayout_15 = QtGui.QVBoxLayout(self.scrollAreaWidgetContents_9)
        self.horizontalLayout_15.setMargin(0)
        self.person_desc = QtGui.QLabel(self.scrollAreaWidgetContents_9)
        self.person_desc.setWordWrap(True)
        self.person_desc.setProperty("desc", "true")
        self.person_desc.setMargin(10)
        self.horizontalLayout_15.addWidget(self.person_desc, 0, QtCore.Qt.AlignTop)
        self.person_desc_scroll.setWidget(self.scrollAreaWidgetContents_9)
        self.horizontalLayout_13.addWidget(self.person_desc_scroll)
        self.person_tabs.addTab(self.person_desc_tab, _fromUtf8(""))
        self.person_roles_tab = QtGui.QWidget()
        self.horizontalLayout_6 = QtGui.QHBoxLayout(self.person_roles_tab)
        self.horizontalLayout_6.setMargin(0)
        self.horizontalLayout_6.setSpacing(0)
        self.scrollArea = QtGui.QScrollArea(self.person_roles_tab)
        self.scrollArea.setWidgetResizable(True)
        self.scrollAreaWidgetContents_4 = QtGui.QWidget()
        self.scrollAreaWidgetContents_4.setGeometry(QtCore.QRect(0, 0, 603, 542))
        self.scrollAreaWidgetContents_4.setProperty("scrollarea", "true")
        self.role_layout = QtGui.QVBoxLayout(self.scrollAreaWidgetContents_4)
        self.role_layout.setContentsMargins(0, 0, 10, 0)
        self.scrollArea.setWidget(self.scrollAreaWidgetContents_4)
        self.horizontalLayout_6.addWidget(self.scrollArea)
        self.person_tabs.addTab(self.person_roles_tab, _fromUtf8(""))
        self.horizontalLayout_14.addWidget(self.person_tabs)
        self.stackedWidget.addWidget(self.person_info)
        self.series_display = QtGui.QWidget()
        self.horizontalLayout_20 = QtGui.QHBoxLayout(self.series_display)
        self.horizontalLayout_20.setMargin(0)
        self.scrollArea_3 = QtGui.QScrollArea(self.series_display)
        self.scrollArea_3.setWidgetResizable(True)
        self.scrollAreaWidgetContents_7 = QtGui.QWidget()
        self.scrollAreaWidgetContents_7.setGeometry(QtCore.QRect(0, 0, 838, 577))
        self.scrollAreaWidgetContents_7.setProperty("scrollarea", "true")
        self.display_layout = QtGui.QVBoxLayout(self.scrollAreaWidgetContents_7)
        self.display_layout.setContentsMargins(0, 0, 10, 0)
        self.display_entry = QtGui.QWidget(self.scrollAreaWidgetContents_7)
        self.horizontalLayout_19 = QtGui.QHBoxLayout(self.display_entry)
        self.horizontalLayout_19.setMargin(0)
        self.display_text = QtGui.QVBoxLayout()
        self.display_title = QtGui.QLabel(self.display_entry)
        self.display_text.addWidget(self.display_title)
        self.display_desc = QtGui.QLabel(self.display_entry)
        self.display_text.addWidget(self.display_desc)
        self.horizontalLayout_19.addLayout(self.display_text)
        spacerItem10 = QtGui.QSpacerItem(448, 20, QtGui.QSizePolicy.Expanding, 
                                         QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_19.addItem(spacerItem10)
        self.display_img = QtGui.QLabel(self.display_entry)
        self.horizontalLayout_19.addWidget(self.display_img)
        self.display_layout.addWidget(self.display_entry)
        self.scrollArea_3.setWidget(self.scrollAreaWidgetContents_7)
        self.horizontalLayout_20.addWidget(self.scrollArea_3)
        self.stackedWidget.addWidget(self.series_display)
        self.main_layout.addWidget(self.stackedWidget)
        self.horizontalLayout_9.addLayout(self.main_layout)
        lanii_main.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(lanii_main)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1144, 27))
        self.menuFile = QtGui.QMenu(self.menubar)
        lanii_main.setMenuBar(self.menubar)
        self.actionImport = QtGui.QAction(lanii_main)
        self.actionTest = QtGui.QAction(lanii_main)
        self.actionExit = QtGui.QAction(lanii_main)
        self.menuFile.addAction(self.actionImport)
        self.menuFile.addAction(self.actionExit)
        self.menubar.addAction(self.menuFile.menuAction())
        self.statusbar = QtGui.QStatusBar(lanii_main)
        lanii_main.setStatusBar(self.statusbar)
        self.loading_gif = QtGui.QLabel()
        self.statusbar.hide()
        
        # Context menu for series list
        self.series_context = QtGui.QMenu(self)
        self.path_action = QtGui.QAction("Change series path", self)
        self.series_context.addAction(self.path_action)
        self.s_import_action = QtGui.QAction("Re-import series", self)
        self.series_context.addAction(self.s_import_action)
        self.s_remove_action = QtGui.QAction("Remove series", self)
        self.series_context.addAction(self.s_remove_action)

        # Context menu for char_list
        self.char_context = QtGui.QMenu(self)
        self.c_import_action = QtGui.QAction("Re-import character", self)
        self.char_context.addAction(self.c_import_action)
        self.c_remove_action = QtGui.QAction("Remove character", self)
        self.char_context.addAction(self.c_remove_action)

        # Context menu for people_list
        self.people_context = QtGui.QMenu(self)
        self.p_import_action = QtGui.QAction("Re-import person", self)
        self.people_context.addAction(self.p_import_action)
        self.p_remove_action = QtGui.QAction("Remove person", self)
        self.people_context.addAction(self.p_remove_action)

        # Context menu for related_entry
        self.related_context = QtGui.QMenu(self)
        self.r_import_action = QtGui.QAction("Import series", self)
        self.related_context.addAction(self.r_import_action)
        
        # Context menu for recommended entry
        self.rec_context = QtGui.QMenu(self)
        self.rec_import_action = QtGui.QAction("Import series", self)
        self.rec_context.addAction(self.rec_import_action)
        
        # OTHER QT WINDOWS/WIDGETS/DIALOGS
        ######################################################################

        # Generic dialog for confirmation
        self.generic_dialog = QConfirm()
        self.generic_dialog.setModal(QtCore.Qt.ApplicationModal)
        self.generic_dialog.buttonBox.accepted.connect(self.generic_dialog.accept)
        self.generic_dialog.hide()

        # EVENT HANDLING
        ###################################################################### 
        # Events for context menu actions
        self.path_action.triggered.connect(self.change_path)
        self.s_import_action.triggered.connect(self.simport_dialog)
        self.s_remove_action.triggered.connect(self.sremove_dialog)
        self.c_import_action.triggered.connect(self.cimport_dialog)
        self.p_import_action.triggered.connect(self.pimport_dialog)
        self.r_import_action.triggered.connect(self.rimport_dialog)
        self.rec_import_action.triggered.connect(self.recimport_dialog)
        self.actionImport.triggered.connect(self.import_dialog)
        self.actionExit.triggered.connect(self.close)
    
        # Events for navigation buttons
        self.button_left.clicked.connect(self.nav_widget_left)
        self.button_right.clicked.connect(self.nav_widget_right)
        
        # Event to play episode when clicked
        self.play_button.clicked.connect(self.play_episode)
        
        # Events to load series/character/person when clicked
        self.series_list.itemPressed.connect(self.load_series_listener)
        self.app_list.itemPressed.connect(self.load_series_listener)
        self.char_list.itemPressed.connect(self.load_char_listener)
        self.people_list.itemPressed.connect(self.load_person_listener)
        self.char_actor_img.clicked.connect(self.char_actor_event)
   
        # Events for search box and search filter
        self.search_filter.activated['QString'].connect(self.set_search_filter)
        self.search_box.textChanged.connect(self.query_search)
        
        # Event to load custom context menus
        self.series_list.customContextMenuRequested.connect(self.series_list_context_menu)
        self.char_list.customContextMenuRequested.connect(self.char_context_menu)
        self.people_list.customContextMenuRequested.connect(self.people_context_menu)
        self.related_area.customContextMenuRequested.connect(self.related_context_menu)
        self.rec_area.customContextMenuRequested.connect(self.rec_context_menu)

        ###################################################################### 

        self.retranslateUi(lanii_main)
        self.tabs.setCurrentIndex(0)
        self.stackedWidget.setCurrentIndex(0)
        self.series_eps_tabs.setCurrentIndex(0)
        self.char_tabs.setCurrentIndex(0)
        self.person_tabs.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(lanii_main)
        
        # Load QSS/CSS styling
        with open(constants.DEFAULT_QSS, "r") as fh:
            lanii_main.setStyleSheet(fh.read())

    # Set default text for widgets
    def retranslateUi(self, lanii_main):
        lanii_main.setWindowTitle(_translate("lanii_main", "lanii", None))
        self.series_list.setSortingEnabled(True)
        self.tabs.setTabText(self.tabs.indexOf(self.series_tab), 
                             _translate("lanii_main", "Series", None))
        self.char_list.setSortingEnabled(True)
        self.tabs.setTabText(self.tabs.indexOf(self.char_tab), 
                             _translate("lanii_main", "Characters", None))
        self.people_list.setSortingEnabled(True)
        self.tabs.setTabText(self.tabs.indexOf(self.people_tab), 
                             _translate("lanii_main", "People", None))
        self.search_filter.setItemText(1, _translate("lanii_main", "Series", None))
        self.search_filter.setItemText(2, _translate("lanii_main", "Characters", None))
        self.search_filter.setItemText(3, _translate("lanii_main", "People", None))
        self.search_filter.setItemText(4, _translate("lanii_main", "Genres", None))
        self.search_filter.setItemText(5, _translate("lanii_main", "Studios", None))
        self.search_filter.setItemText(6, _translate("lanii_main", "Producers", None))
        self.search_filter.setItemText(7, _translate("lanii_main", "Licensors", None))
        self.title_label.setText(_translate("lanii_main", "Title", None))
        self.subtitle.setText(_translate("lanii_main", "Runtime and # of episodes", None))
        self.button_left.setText(_translate("lanii_main", "<", None))
        self.button_right.setText(_translate("lanii_main", ">", None))
        self.episode_list.setSortingEnabled(True)
        self.series_eps_tabs.setTabText(self.series_eps_tabs.indexOf(self.episode_tab), 
                                        _translate("lanii_main", "Episodes", None))
        self.series_eps_tabs.setTabText(self.series_eps_tabs.indexOf(self.related_tab), 
                                        _translate("lanii_main", "Related", None))
        self.series_eps_tabs.setTabText(self.series_eps_tabs.indexOf(self.tab), 
                                        _translate("lanii_main", "Recommended", None))
        self.play_button.setText(_translate("lanii_main", "Play", None))
        self.char_scroll_title.setText(_translate("lanii_main", "Characters", None))
        self.staff_scroll_title.setText(_translate("lanii_main", "Staff", None))
        self.char_tabs.setTabText(self.char_tabs.indexOf(self.char_desc_tab), 
                                  _translate("lanii_main", "Description", None))
        self.app_list.setSortingEnabled(True)
        self.char_tabs.setTabText(self.char_tabs.indexOf(self.char_apps), 
                                  _translate("lanii_main", "Appearances", None))
        self.person_desc.setText(_translate("lanii_main", "TextLabel", None))
        self.person_tabs.setTabText(self.person_tabs.indexOf(self.person_desc_tab), 
                                    _translate("lanii_main", "Description", None))
        self.person_tabs.setTabText(self.person_tabs.indexOf(self.person_roles_tab), 
                                    _translate("lanii_main", "Roles", None))
        self.display_title.setText(_translate("lanii_main", "Title", None))
        self.display_desc.setText(_translate("lanii_main", "Relation", None))
        self.display_img.setText(_translate("lanii_main", "Img", None))
        self.menuFile.setTitle(_translate("lanii_main", "File", None))
        self.actionImport.setText(_translate("lanii_main", "Import series", None))
        self.actionTest.setText(_translate("lanii_main", "test", None))
        self.actionExit.setText(_translate("lanii_main", "Quit", None))
        self.search_box.setPlaceholderText("Search")

    ################################################################################
    ##### Drawing Info to Window #####
    ################################################################################

    def populate_list(self, qlist, table):
        """
        Clear and populate a QListWidget with data from the table
        Table must be a list of either: Series, Person, or Character
        """
        qlist.clear()
        for item in table:
            # Ignore the dummy entry
            if item['id'] != 0:
                new_item = QtGui.QListWidgetItem()
                new_item.setText(item['name'])
                qlist.addItem(new_item)

    def populate_char_list(self, table):
        """
        Clear and populate the char_list with cleaned character names
        """
        self.char_list.clear()
        for char in table:
            new_item = QtGui.QListWidgetItem()
            name = sformat.clean_name(char['name']).strip()
            new_item.setText(name)
            self.char_list.addItem(new_item)

    def populate_ep_list(self, path):
        """
        Clear and populate the ep_list with cleaned episode names
        """
        self.episode_list.clear()
        for ep in tools.valid_files(path,0):
            if tools.valid_episode(ep):
                ep_name = sformat.clean_episode(ep)
                new_item = QtGui.QListWidgetItem()
                new_item.setText(ep_name)
                self.episode_list.addItem(new_item)

    def char_name_map(self):
        """
        Create a dictionary of shortened name -> actual name
        """
        char_names = {}
        for char in db.get_all_chars():
            name = sformat.clean_name(char['name']).strip()
            char_names[name] = char['name']
        return char_names
    
    def ep_name_map(self, path):
        """
        Create a dictionary of filename -> episode name
        """
        ep_names = {}
        for ep in tools.valid_files(path,0):
            if tools.valid_episode(ep):
                ep_name = sformat.clean_episode(ep)
                ep_names[ep_name] = ep
        return ep_names
    
    def refresh(self):
        """
        Repopulates the series_list, char_list, and people_list
        """
        # Populate the series_list
        table = db.get_all_series()
        if table:
            self.populate_list(self.series_list, table)

        # Populate the char_list
        table = db.get_all_chars()
        if table:
            self.populate_char_list(table)
        self.char_names = self.char_name_map()

        # Populate the people_list
        table = db.get_all_people()
        if table:
            self.populate_list(self.people_list, table)
        
        # TODO: Test this. When importing a series from empty, after import is
        #       done the series should automatically show
        if self.series_list.count() == 1:
            self.series_list.setCurrentRow(0)
            self.load_series_listener(self.series_list.currentItem())
   
    def populate(self):
        """
        Runs everytime main window inits to populate main QListWidgets
        """
        self.refresh()
        # Setting focus on title removes weird box inside tabs for self.tabs
        self.title_label.setFocus()
        # Set current_series for video playing
        if self.series_list.count():
            # Select the first item by default
            self.load_series_listener(self.series_list.item(0))
            self.series_list.setCurrentRow(0)

    def convert_image(self, img_file, img_size):
        """
        Apply an image to a label
        Args:
            img_file        The path of the image file
            img_size        Size of the image (width, height)
        """
        img = Image.open(img_file)
        if not img_size == self.FULL_IMG_SIZE:
            # Transform image to size
            img.thumbnail(img_size, Image.ANTIALIAS)
            # Convert image to approx. size, then evenly crop each side to target res.
            v_size = img.getbbox()
            wdiff = (v_size[2] - img_size[0])/2
            hdiff = (v_size[3] - img_size[1])/2
            area = (0+wdiff, 0+hdiff, v_size[2]-wdiff, v_size[3]-hdiff)
            img = img.crop(area)
        # Load image as a pixmap for a QLabel
        qimg = ImageQt(img)
        # .copy() necessary according to StackOverflow
        myPixmap = QtGui.QPixmap.fromImage(qimg).copy()
        return myPixmap
    
    def build_image(self, img_file, img_size, label):
        """
        Apply an image to a label
        Args:
            img_file        The path of the image file
            img_size        Size of the image (width, height)
            label           QLabel to apply image on
        """
        pixmap = self.convert_image(img_file, img_size)
        label.setPixmap(pixmap)
        return label
    
    def build_icon(self, img_file, img_size, button):
        """
        Apply an icon (image) to a QButton
        Args:
            img_file        The path of the image file
            img_size        Size of the image (width, height)
            label           QButton to apply image on
        """
        pixmap = self.convert_image(img_file, img_size)
        icon = QtGui.QIcon()
        icon.addPixmap(pixmap)
        button.setIcon(icon)
        button.setIconSize(pixmap.rect().size())
        return button
    
    def set_dialog(self, dialog, accept, msg):
        """
        Set up a message and accept action to a dialog box
        Args:
            dialog          Dialog box to apply to
            accept          Name of function to run when accepted
            msg             A message (string) to display
        """
        dialog.set_msg(msg)
        dialog.set_accept(accept)
        dialog.show()

    ################################################################################
    ##### Imports and Threading #####
    ################################################################################

    # Given a thread, connect signals and start
    def execute_thread(self, thread):
        # Create a thread object for import operations, passing the target folder 
        thread.statusMessage.connect(self.print_status)
        thread.hideGif.connect(self.hide_loading)
        thread.refresh.connect(self.refresh)
        thread.finished.connect(self.exit_thread)
        # Ensure only one import thread at a time
        self.thread_exists = True
        # Start the thread
        thread.start()
    
    # Stop gif, hide statusbar, delete current thread, and allow new threads to be created
    def exit_thread(self):
        self.loading_gif.movie().stop()
        self.statusbar.hide()
        self.import_thread.quit()
        self.import_thread.wait()
        self.import_thread.deleteLater()
        self.thread_exists = False

    # Create a thread for importing a series and execute it
    def import_series(self, folder):
        # Ensure no other theads currently active
        if not self.thread_exists:
            self.import_thread = QImportSeries(folder)
            self.execute_thread(self.import_thread)
    
    # Create a thread for importing a character and execute
    def import_character(self, name):
        # Ensure no other threads currently active
        if not self.thread_exists:
            self.import_thread = QImportChar(name)
            self.execute_thread(self.import_thread)
    
    # Create a thread for importing a person and execute
    def import_person(self, name):
        # Ensure no other threads currently active
        if not self.thread_exists:
            self.import_thread = QImportPerson(name)
            self.execute_thread(self.import_thread)
    
        
    ################################################################################
    ##### Widget/Entry Building #####
    ################################################################################
    
    # Reset a layout by deleting it and creating a new version in the container
    def reset_layout(self, layout, container):
        self.clear_layout(layout)
        sip.delete(layout)
        new_layout = QtGui.QVBoxLayout(container)
        new_layout.setContentsMargins(0, 0, 0, 0)
        new_layout.setSpacing(0)
        return new_layout

    # Recursively delete all items in a layout
    def clear_layout(self, layout):
        while layout.count():
            child = layout.takeAt(0)
            if child.widget() is not None:
                child.widget().deleteLater()
            elif child.layout() is not None:
                clear_layout(child.layout())

    # Create a new related entry for a series
    def new_related_entry(self, title, relation, img):
        related_entry = QEntry(self.scrollAreaWidgetContents_5)
        related_entry.setProperty("entry", "true")
        related_entry.setProperty("related", "true")
        related_entry.setContentsMargins(6,0,6,1)
        related_entry.setMaximumSize(QtCore.QSize(16777215, 136))
        horizontalLayout_16 = QtGui.QHBoxLayout(related_entry)
        horizontalLayout_16.setMargin(0)
        related_text = QtGui.QVBoxLayout()
        related_relation = QtGui.QLabel(related_entry)
        related_relation.setProperty("subentry-title", "true")
        related_relation.setMinimumSize(QtCore.QSize(500, 30))
        related_relation.setMaximumSize(QtCore.QSize(16777215, 30))
        related_relation.setText(relation)
        related_text.addWidget(related_relation)
        related_title = QtGui.QLabel(related_entry)
        related_title.setProperty("entry-title", "true")
        related_title.setMaximumSize(QtCore.QSize(530,167777))
        title = sformat.format_entry_title(title)
        related_title.setText(title)
        related_text.addWidget(related_title)
        horizontalLayout_16.addLayout(related_text)
        spacerItem2 = QtGui.QSpacerItem(467, 20, QtGui.QSizePolicy.Expanding, 
                                        QtGui.QSizePolicy.Minimum)
        horizontalLayout_16.addItem(spacerItem2)
        related_img = QtGui.QLabel(related_entry)
        related_img = self.build_image(img, self.SMALL_IMG_SIZE, related_img)
        horizontalLayout_16.addWidget(related_img)
        related_entry.setInfo(related_title)
        related_entry.clicked.connect(self.series_entry_event)
        related_entry.right_clicked.connect(self.related_rclick)
        return related_entry
    
    # Create a new rec entry for a series
    def new_rec_entry(self, title, img):
        rec_entry = QEntry(self.scrollAreaWidgetContents_6)
        rec_entry.setProperty("entry", "true")
        rec_entry.setProperty("related", "true")
        rec_entry.setContentsMargins(6,0,6,1)
        rec_entry.setMaximumSize(QtCore.QSize(16777215, 136))
        horizontalLayout_17 = QtGui.QHBoxLayout(rec_entry)
        horizontalLayout_17.setMargin(0)
        rec_title = QtGui.QLabel(rec_entry)
        rec_title.setProperty("entry-title", "true")
        title = sformat.format_entry_title(title)
        rec_title.setText(title)
        horizontalLayout_17.addWidget(rec_title)
        spacerItem3 = QtGui.QSpacerItem(448, 20, QtGui.QSizePolicy.Expanding, 
                                        QtGui.QSizePolicy.Minimum)
        horizontalLayout_17.addItem(spacerItem3)
        rec_img = QtGui.QLabel(rec_entry)
        rec_img = self.build_image(img, self.SMALL_IMG_SIZE, rec_img)
        horizontalLayout_17.addWidget(rec_img)
        rec_entry.setInfo(rec_title)
        rec_entry.clicked.connect(self.series_entry_event)
        rec_entry.right_clicked.connect(self.rec_rclick)
        return rec_entry

    # Create a new char entry for a series
    def new_char_entry(self, cname, cimg, role, aname, aimg):
        char_entry = QtGui.QWidget(self.scrollAreaWidgetContents_2)
        char_entry.setContentsMargins(0,0,0,1)
        char_entry.setProperty("entry-cast", "true")
        char_entry.setProperty("related", "true")
        verticalLayout_3 = QtGui.QVBoxLayout(char_entry)
        verticalLayout_3.setMargin(0)
        char_entry_name_layout = QtGui.QHBoxLayout()
        char_name = QtGui.QLabel(char_entry)
        char_name.setProperty("entry", "true")
        char_name.setProperty("entry-cast", "true")
        char_name.setText(cname)
        char_entry_name_layout.addWidget(char_name)
        spacerItem4 = QtGui.QSpacerItem(0, 20, QtGui.QSizePolicy.Expanding, 
                                        QtGui.QSizePolicy.Minimum)
        char_entry_name_layout.addItem(spacerItem4)
        actor_name = QtGui.QLabel(char_entry)
        actor_name.setProperty("entry", "true")
        actor_name.setProperty("entry-cast", "true")
        actor_name.setText(aname)
        char_entry_name_layout.addWidget(actor_name)
        verticalLayout_3.addLayout(char_entry_name_layout)
        char_role = QtGui.QLabel(char_entry)
        char_role.setProperty("subentry", "true")
        char_role.setMaximumSize(QtCore.QSize(450, 100))
        char_role.setText(role)
        verticalLayout_3.addWidget(char_role)
        char_entry_pic_layout = QtGui.QHBoxLayout()

        char_pic_img = QEPushButton(char_entry)
        char_pic_img.setProperty("entry-pic", "true")
        char_pic_img.setProperty("entry-left", "true")
        char_pic_img.setObjectName(cname)
        char_pic_img = self.build_icon(cimg, self.SMALL_IMG_SIZE, char_pic_img)
        char_pic_img.clicked.connect(self.char_load_event)
        char_entry_pic_layout.addWidget(char_pic_img)

        spacerItem5 = QtGui.QSpacerItem(0, 20, QtGui.QSizePolicy.Expanding, 
                                        QtGui.QSizePolicy.Minimum)

        char_entry_pic_layout.addItem(spacerItem5)
        actor_pic_img = QEPushButton(char_entry)
        actor_pic_img.setProperty("entry-pic", "true")
        actor_pic_img.setProperty("entry-right", "true")
        actor_pic_img.setObjectName(aname)
        actor_pic_img = self.build_icon(aimg, self.SMALL_IMG_SIZE, actor_pic_img)
        actor_pic_img.clicked.connect(self.cast_person_event)
        char_entry_pic_layout.addWidget(actor_pic_img)

        verticalLayout_3.addLayout(char_entry_pic_layout)

        return char_entry

    # Create a new staff entry for a series
    def new_staff_entry(self, name, role, img):
        staff_entry = QtGui.QWidget(self.scrollAreaWidgetContents_3)
        staff_entry.setMinimumSize(QtCore.QSize(0, 219))
        staff_entry.setProperty("entry-cast", "true")
        staff_entry.setProperty("related", "true")
        vL= QtGui.QVBoxLayout(staff_entry)
        vL.setMargin(0)
        nsll = QtGui.QVBoxLayout()
        staff_name = QtGui.QLabel(staff_entry)
        staff_name.setProperty("entry", "true")
        staff_name.setProperty("staff-entry", "true")
        staff_name.setMaximumSize(QtCore.QSize(16777215, 52))
        staff_name.setText(name)
        nsll.addWidget(staff_name)
        nspl = QtGui.QHBoxLayout()
        staff_img = QEPushButton(staff_entry)
        staff_img = self.build_icon(img, self.SMALL_IMG_SIZE, staff_img)
        staff_img.setObjectName(name)
        staff_img.setProperty("entry-pic", "true")
        staff_img.setProperty("entry-left", "true")
        staff_img.clicked.connect(self.cast_person_event)
        nspl.addWidget(staff_img)
        sp = QtGui.QSpacerItem(0, 20, QtGui.QSizePolicy.Expanding, 
                               QtGui.QSizePolicy.Minimum)
        nspl.addItem(sp)
        staff_role = QtGui.QLabel(staff_entry)
        staff_role.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|
                                QtCore.Qt.AlignVCenter)
        staff_role.setProperty("subentry-staff", "true")
        display_role = sformat.clean_role(role)
        staff_role.setText(display_role)
        staff_role.setProperty("role", "true")
        nspl.addWidget(staff_role)
        nsll.addLayout(nspl)
        vL.addLayout(nsll)

        return staff_entry
    
    # Create a new role entry for a person
    def new_role_entry(self, series, char, role, s_img, c_img):
        role_entry = QtGui.QWidget(self.scrollAreaWidgetContents_4)
        role_entry.setProperty("related", "true")
        role_entry.setProperty("entry-cast", "true")
        role_entry.setMaximumSize(QtCore.QSize(16777215, 250))
        verticalLayout_4 = QtGui.QVBoxLayout(role_entry)
        verticalLayout_4.setMargin(0)
        char_entry_name_layout_3 = QtGui.QHBoxLayout()
        series_name = QtGui.QLabel(role_entry)
        series_name.setProperty("entry", "true")
        series_name.setProperty("entry-cast", "true")
        series_name.setText(series)
        char_entry_name_layout_3.addWidget(series_name)
        spacerItem8 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, 
                                        QtGui.QSizePolicy.Minimum)
        char_entry_name_layout_3.addItem(spacerItem8)
        char_name = QtGui.QLabel(role_entry)
        char_name.setProperty("entry", "true")
        char_name.setProperty("entry-cast", "true")
        char_name.setText(char)
        char_entry_name_layout_3.addWidget(char_name)
        verticalLayout_4.addLayout(char_entry_name_layout_3)
        char_role = QtGui.QLabel(role_entry)
        char_role.setProperty("subentry", "true")
        char_role.setText(role)
        verticalLayout_4.addWidget(char_role)
        actor_app_img_layout = QtGui.QHBoxLayout()
        actor_app_series_pic = QEPushButton(role_entry)
        actor_app_series_pic.setProperty("entry-pic", "true")
        actor_app_series_pic.setProperty("entry-left", "true")
        actor_app_series_pic = self.build_icon(s_img, self.SMALL_IMG_SIZE, 
                                               actor_app_series_pic)
        actor_app_series_pic.setObjectName(series)
        actor_app_series_pic.clicked.connect(self.role_series_event)
        actor_app_img_layout.addWidget(actor_app_series_pic)
        spacerItem9 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, 
                                        QtGui.QSizePolicy.Minimum)
        actor_app_img_layout.addItem(spacerItem9)
        actor_app_char_pic = QEPushButton(role_entry)
        actor_app_char_pic.setProperty("entry-pic", "true")
        actor_app_char_pic.setProperty("entry-right", "true")
        actor_app_char_pic.setObjectName(char)
        actor_app_char_pic = self.build_icon(c_img, self.SMALL_IMG_SIZE, 
                                              actor_app_char_pic)
        actor_app_char_pic.clicked.connect(self.char_load_event)
        actor_app_img_layout.addWidget(actor_app_char_pic)
        verticalLayout_4.addLayout(actor_app_img_layout)
        return role_entry

    # Create a new staff position entry for a person
    def new_staff_pos_entry(self, series, position, s_img):
        role_entry = QEntry(self.scrollAreaWidgetContents_4)
        role_entry.setContentsMargins(6,0,6,1)
        role_entry.setProperty("entry-cast", "true")
        role_entry.setProperty("related", "true")
        role_entry.setMaximumSize(QtCore.QSize(16777215, 180))
        verticalLayout_4 = QtGui.QVBoxLayout(role_entry)
        verticalLayout_4.setMargin(0)
        char_entry_name_layout_3 = QtGui.QHBoxLayout()
        series_name = QtGui.QLabel(role_entry)
        series_name.setProperty("subentry-title", "true")
        series_name.setText(series)
        series_name.setMinimumSize(QtCore.QSize(625, 30))
        series_name.setMaximumSize(QtCore.QSize(16777215, 30))
        char_entry_name_layout_3.addWidget(series_name)
        spacerItem8 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, 
                                        QtGui.QSizePolicy.Minimum)
        char_entry_name_layout_3.addItem(spacerItem8)
        verticalLayout_4.addLayout(char_entry_name_layout_3)
        actor_app_img_layout = QtGui.QHBoxLayout()
        role = QtGui.QLabel(role_entry)
        role.setProperty("entry-title", "true")
        role.setText(position)
        actor_app_img_layout.addWidget(role)
        spacerItem9 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, 
                                        QtGui.QSizePolicy.Minimum)
        actor_app_img_layout.addItem(spacerItem9)
        actor_app_series_pic = QtGui.QLabel(role_entry)
        actor_app_series_pic.setProperty("entry-pic", "true")
        actor_app_series_pic = self.build_image(s_img, self.SMALL_IMG_SIZE, 
                                                actor_app_series_pic)
        actor_app_img_layout.addWidget(actor_app_series_pic)
        verticalLayout_4.addLayout(actor_app_img_layout)
        role_entry.setInfo(series_name)
        role_entry.clicked.connect(self.series_entry_event)
        return role_entry

    # For a series id, create and add related series entries to the series_eps page
    def add_related_entries(self, series):
        for pre in series['prequels']:
            pre_entry = self.new_related_entry(pre[0], "Prequel", pre[1])
            self.related_layout.addWidget(pre_entry, 0, QtCore.Qt.AlignTop)

        for seq in series['sequels']:
            seq_entry = self.new_related_entry(seq[0], "Sequel", seq[1])
            self.related_layout.addWidget(seq_entry, 0, QtCore.Qt.AlignTop)

        for alt in series['alternates']:
            alt_entry = self.new_related_entry(alt[0], "Alternative", alt[1])
            self.related_layout.addWidget(alt_entry, 0, QtCore.Qt.AlignTop)
        
        for side in series['sides']:
            side_entry = self.new_related_entry(side[0], "Side story", side[1])
            self.related_layout.addWidget(side_entry, 0, QtCore.Qt.AlignTop)
        
        for oth in series['others']:
            oth_entry = self.new_related_entry(oth[0], "Others", oth[1])
            self.related_layout.addWidget(oth_entry, 0, QtCore.Qt.AlignTop)

        spacerItem = QtGui.QSpacerItem(0, 400, QtGui.QSizePolicy.Minimum, 
                                        QtGui.QSizePolicy.Expanding)
        self.related_layout.addItem(spacerItem)
    
    # For a series id, create and add recommended series entries to the series_eps page
    def add_rec_entries(self, series):
        for entry in series['recs']:
            rec_entry = self.new_rec_entry(entry[0], entry[1])
            self.rec_layout.addWidget(rec_entry)

        spacerItem = QtGui.QSpacerItem(0, 400, QtGui.QSizePolicy.Minimum, 
                                        QtGui.QSizePolicy.Expanding)
        self.rec_layout.addItem(spacerItem)

    # For a series id, create and add character entries to the series_cast page
    def add_char_entries(self, s_id):
        for entry in db.cast_from_series(s_id):
            cname_display = sformat.clean_name(entry[0])
            # Build a new entry and add to layout
            char_entry = self.new_char_entry(cname_display, entry[1], entry[2], 
                                             entry[3], entry[4])
            self.char_layout.addWidget(char_entry)

    # For a series id, create and add staff entries to the series_cast page
    def add_staff_entries(self, s_id):
        for staff in db.staff_from_series(s_id):
            # Build a new entry and add to layout
            staff_entry = self.new_staff_entry(staff[0], staff[1], staff[2])
            self.staff_layout.addWidget(staff_entry)
    
    # Clear the entries for the cast of a series
    def clear_cast_entries(self):
        self.char_layout = self.reset_layout(self.char_layout,
                                             self.scrollAreaWidgetContents_2)
        self.staff_layout = self.reset_layout(self.staff_layout,
                                              self.scrollAreaWidgetContents_3)
    
    # Build the entries for the cast of a series
    def add_cast_entries(self, series_id):
        # Remember the current cast series displayed
        self.cast_entry_series = series_id

        # Clear all entries first
        self.clear_cast_entries()

        # Add char_entries to char_layout
        self.add_char_entries(series_id)
        
        # Add staff_entries to staff_layout
        self.add_staff_entries(series_id)

    # For a person, create role entries detailing what they have done
    def add_role_entries(self, name):
        person = db.person_from_name(name)
        for role in person['roles']:
            desc = role[3]
            series = db.series_from_id(role[0])
            char = db.char_from_id(role[1])
            role_entry = self.new_role_entry(series['name'], char['name'], desc, 
                                             series['image'], char['image'])
            self.role_layout.addWidget(role_entry, 0, QtCore.Qt.AlignTop)
    
    # For a person, add staff position entries
    def add_staff_pos_entries(self, name):
        person = db.person_from_name(name)
        for p in person['positions']:
            desc = p[2]
            series = db.series_from_id(p[0])
            staff_entry = self.new_staff_pos_entry(series['name'], desc, series['image'])
            self.role_layout.addWidget(staff_entry, 0, QtCore.Qt.AlignTop)

        # Top off with a spacer
        spacerItem = QtGui.QSpacerItem(0, 400, QtGui.QSizePolicy.Minimum, 
                                       QtGui.QSizePolicy.Expanding)
        self.role_layout.addItem(spacerItem)
        
    ################################################################################
    ##### Event Handlers #####
    ################################################################################
    
    #--------------------------------------------------------------------------------
    # Navigation Events
    #--------------------------------------------------------------------------------
    
    # Load only the pages related to a series (first 3)
    def nav_widget_left(self):
        next_index = (self.stackedWidget.currentIndex() - 1) % 3
        # Build cast entries when going to that page and not already built
        if next_index == 2 and self.cast_entry_series != self.current_series:
            self.add_cast_entries(self.current_series)
        self.stackedWidget.setCurrentIndex(next_index)
    
    # Load only the pages related to a series (first 3)
    def nav_widget_right(self):
        next_index = (self.stackedWidget.currentIndex() + 1) % 3
        # Build cast entries when going to that page and not already built
        if next_index == 2 and self.cast_entry_series != self.current_series:
            self.add_cast_entries(self.current_series)
        self.stackedWidget.setCurrentIndex(next_index)

    # Load the series based on the title
    def series_load_event(self, title):
        # Check if series exists
        if db.series_from_title(title):
            # Load series, change pages
            self.load_series(title)
            self.stackedWidget.setCurrentIndex(0)
            self.series_eps_tabs.setCurrentIndex(0)
            self.title_label.setFocus()
        else:
            # Throw an error in status bar?
            pass
    
    # For a series entry, load the series when clicked
    def series_entry_event(self, info):
        title = info.text()
        self.series_load_event(title)
    
    # For a role entry, load the series when clicked
    def role_series_event(self, info):
        title = self.sender().objectName()
        self.series_load_event(title)
    
    # Load a character page after a click event
    def char_load_event(self, info):
        # Name of character is the objectName of the sender
        tmp_name = self.sender().objectName()
        tmp_name = sformat.clean_name(tmp_name)
        # Do mapping of name
        name = self.char_names[tmp_name]
        # Check if char exists
        if db.char_from_name(name):
            # Load char
            self.load_char(tmp_name)
            self.title_label.setFocus()
            # Set the tab back to the first page
            self.char_tabs.setCurrentIndex(0)
        else:
            # Throw an error in status bar?
            pass
    
    # Load an person page after a click event
    def actor_load_event(self, name):
        # Check if person exists
        person = db.person_from_name(name)
        if person['id'] != 0:
            # Load person
            self.load_person(name)
            self.title_label.setFocus()
            # Set the tab back to the first page
            self.person_tabs.setCurrentIndex(0)
        else:
            # Throw an error in status bar?
            pass

    # When a person's image is clicked on the cast page
    def cast_person_event(self, info):
        name = self.sender().objectName()
        self.actor_load_event(name)
    
    # When the actor image is clicked on the character page
    def char_actor_event(self, info):
        # Unformat the name and load actor
        name = sformat.revert_actor_name(self.char_actor_name.text())
        self.actor_load_event(name)

    #-------------------------------------------------------------------------------
    # Context Menu and Dialog Events
    #-------------------------------------------------------------------------------

    # Create the file dialog box when importing a series
    def import_dialog(self, checked):
        # Open file dialog box and get the selected directory (only allow directories)
        series_folder = QtGui.QFileDialog.getExistingDirectory(
                            self, 
                            'Select a folder containing video files for a show/movie', 
                            constants.USER_HOME, 
                            QtGui.QFileDialog.ShowDirsOnly
                        )
        # Import the series if a directory was selected
        if series_folder:
            self.import_series(series_folder)

    # Change the path for a given series
    def change_path(self, item):
        series_folder = QtGui.QFileDialog.getExistingDirectory(
                            self, 
                            'Select the new series folder', 
                            constants.USER_HOME, 
                            QtGui.QFileDialog.ShowDirsOnly
                        )
        if series_folder:
            series_name = self.series_list.currentItem().text()
            db.change_series_path(series_name, series_folder)
            self.load_series(series_name)

    # When a related entry is right-clicked, store text to prepare for importing
    def related_rclick(self, info):
        self.related_series = info.text()

    # When a rec entry is right-clicked, store text to prepare for importing
    def rec_rclick(self, info):
        self.rec_series = info.text()

    # Open the series context menu at pos
    def series_list_context_menu(self, pos):
        pos += QtCore.QPoint(2,0)
        self.series_context.exec_(self.series_list.mapToGlobal(pos))
    
    # Open the related context menu at pos
    def related_context_menu(self, pos):
        pos += QtCore.QPoint(2,0)
        self.related_context.exec_(self.related_area.mapToGlobal(pos))
    
    # Open the recommended context menu at pos
    def rec_context_menu(self, pos):
        pos += QtCore.QPoint(2,0)
        self.rec_context.exec_(self.rec_area.mapToGlobal(pos))

    # Open the character context menu at pos
    def char_context_menu(self, pos):
        pos += QtCore.QPoint(2,0)
        self.char_context.exec_(self.char_list.mapToGlobal(pos))
    
    # Open the people context menu at pos
    def people_context_menu(self, pos):
        pos += QtCore.QPoint(2,0)
        self.people_context.exec_(self.people_list.mapToGlobal(pos))

    # Display the dialog for re-importing a series
    def simport_dialog(self, item):
        self.set_dialog(self.generic_dialog,
                        self.series_reimport,
                        "Are you sure you want to re-import the series?")
    
    # Display the dialog for removing a series
    def sremove_dialog(self, item):
        self.set_dialog(self.generic_dialog,
                        self.remove_series,
                        "Are you sure you want to remove the series?")
    
    # Display the dialog for re-importing a character
    def cimport_dialog(self, item):
        self.set_dialog(self.generic_dialog,
                        self.char_reimport,
                        "Are you sure you want to re-import the character?")
    
    # Display the dialog for re-importing a character
    def cremove_dialog(self, item):
        self.set_dialog(self.generic_dialog,
                        self.remove_char,
                        "Are you sure you want to remove the character?")

    # Display the dialog for re-importing a person
    def pimport_dialog(self, item):
        self.set_dialog(self.generic_dialog,
                        self.person_reimport,
                        "Are you sure you want to re-import the person?")
   
    # Display the dialog for re-importing a person
    def premove_dialog(self, item):
        self.set_dialog(self.generic_dialog,
                        self.remove_person,
                        "Are you sure you want to remove the person?")
 
    # Display confirmation dialog for importing a related series
    def rimport_dialog(self, item):
        self.set_dialog(self.generic_dialog,
                        self.related_import,
                        "Are you sure you want to import the related series?")
    
    # Display confirmation dialog for importing a recommended series
    def recimport_dialog(self, item):
        self.set_dialog(self.generic_dialog,
                        self.rec_import,
                        "Are you sure you want to import the recommended series?")

    # Re-import a series
    def series_reimport(self):
        self.generic_dialog.hide()
        title = self.series_list.currentItem().text()
        series = db.series_from_title(title)
        path = series.path
        self.import_series(path)
        self.refresh()
    
    # TODO
    # Remove a series from the database along with sequels, etc.:
    #   - Related, connections to producer/licensors/etc., 
    def remove_series(self):
        self.generic_dialog.hide()
        title = self.series_list.currentItem().text()
        print("Deleting: " + title)
        #db.remove_series(title)
        self.refresh()

    # Re-import a character
    def char_reimport(self):
        self.generic_dialog.hide()
        name = self.char_list.currentItem().text()
        full_name = self.char_names[name]
        self.import_character(full_name)
        self.refresh()
    
    # TODO
    # Remove a character from the database
    def remove_char(self):
        name = self.char_list.currentItem().text()
        full_name = self.char_names[name]
        #db.remove_char(name)
        self.refresh()
        pass

    # Re-import a person
    def person_reimport(self):
        self.generic_dialog.hide()
        name = self.people_list.currentItem().text()
        self.import_person(name)
        self.refresh()
    
    # TODO
    # Remove a person from the program
    def remove_person(self):
        name = self.people_list.currentItem().text()
        #db.remove_person(name)
        pass

    # Import a related series
    def related_import(self):
        self.generic_dialog.hide()
        # Import the related series that was right-clicked
        self.import_series(self.related_series)
        self.refresh()
    
    # Import a recommended series
    def rec_import(self):
        self.generic_dialog.hide()
        # Import the related series that was right-clicked
        self.import_series(self.rec_series)
        self.refresh()

    #-------------------------------------------------------------------------------
    # Statusbar Events
    #-------------------------------------------------------------------------------

    # Ensure that the status bar is visible and gif plays, then show status message
    def print_status(self, msg, timer):
        if not self.statusbar.isVisible():
            self.statusbar.show()
        if not self.loading_gif.movie().state() == QtGui.QMovie.Running:
            self.loading_gif.movie().start()
        if self.statusbar.currentMessage():
            self.statusbar.clearMessage()
        self.statusbar.showMessage(msg, timer)
    
    # Hide the loading gif on the status bar
    # TODO: Find a way to hide the gif. Currently just stops it from playing
    def hide_loading(self):
        self.loading_gif.movie().stop()

    #-------------------------------------------------------------------------------
    # Searching Events
    #-------------------------------------------------------------------------------

    # When the user changes the search filter, call the search function again
    def set_search_filter(self, set_filter):
        self.query_search(self.search_box.text())

    def query_search(self, query):
        """
        Responds to changes in the search text box or search filter
        Given the filter and text, return and display all relevant entries from the
        database to the QListWidgets
        """
        if query:
            if self.search_filter.currentIndex() == 1:
                # Series filter
                series = db.search_series(query)
                self.populate_list(self.series_list, series)
            elif self.search_filter.currentIndex() == 2:
                # Characters filter
                chars = db.search_chars(query)
                self.populate_char_list(chars)
            elif self.search_filter.currentIndex() == 3:
                # People filter
                people = db.search_people(query)
                self.populate_list(self.people_list, people)
            elif self.search_filter.currentIndex() == 4:
                # Genres filter
                series = db.search_genres(query)
                self.populate_list(self.series_list, series)
            elif self.search_filter.currentIndex() == 5:
                # Studios filter
                series = db.search_studios(query)
                self.populate_list(self.series_list, series)
            elif self.search_filter.currentIndex() == 6:
                # Producers filter
                series = db.search_producers(query)
                self.populate_list(self.series_list, series)
            elif self.search_filter.currentIndex() == 7:
                # Licensors filter
                series = db.search_licensors(query)
                self.populate_list(self.series_list, series)
            else:
                # Retrieve all chars, people, and series info for the query
                chars = db.search_chars(query)
                people = db.search_people(query)
                series = db.search_series(query)

                # Populate lists with info
                self.populate_list(self.series_list, series)
                self.populate_char_list(chars)
                self.populate_list(self.people_list, people)
        else:
            # If there is nothing in the search bar, just show everything
            # If an item in the list was selected, remember the text
            if self.series_list.currentRow() >= 0:
                s_name = self.series_list.currentItem().text()
            if self.char_list.currentRow() >= 0:
                c_name = self.char_list.currentItem().text()
            if self.people_list.currentRow() >= 0:
                p_name = self.people_list.currentItem().text()
            # Refresh all entries in lists (i.e. add them all back)
            self.refresh()
        
        # Set current item for the 3 main QListWidgets from class state variables
        try:
            c_item = self.get_list_item(self.char_list, self.load_char_state)
            self.char_list.setCurrentItem(c_item)
        except IndexError:
            pass
        try:
            p_item = self.get_list_item(self.people_list, self.load_person_state)
            self.people_list.setCurrentItem(p_item)
        except IndexError:
            pass
        try:
            s_item = self.get_list_item(self.series_list, self.load_series_state)
            self.series_list.setCurrentItem(s_item)
        except IndexError:
            pass

    def get_list_item(self, list_, name):
        """
        Return the QListWidgetItem that has the string 'name' from QListWidget
        """
        return list_.findItems(name, QtCore.Qt.MatchExactly)[0]

    #-------------------------------------------------------------------------------   
    # "Load" Events
    #-------------------------------------------------------------------------------   

    def load_series_listener(self, item):
        """
        An interface to load_series(), reacts to events that want to call it
        """
        # Prevent unintended calls
        if not item:
            return
        # Get name of series from widget and load
        self.load_series(item.text())

    def load_series(self, title):
        """
        Load information about a series from the title and builds the page
        """
        # Remember as most recently loaded series
        self.load_series_state = title

        # Retrieve series info from database
        series = db.series_from_title(title)

        # Set path to prepare for video play
        self.series_path = series['path']
        
        ### Set series_eps page ###
        # Maintain page when switching between series
        index = self.stackedWidget.currentIndex()
        if index > 2:
            self.stackedWidget.setCurrentIndex(0)
        # Set all information
        self.button_right.show()
        self.button_left.show()
        self.current_series = series['id']
        
        # Set title, store it, and set focus
        self.title_label.setText(title)
        self.title_label.setFocus()
    
        # Create subtitle, set it, and store it
        subtitle_text = sformat.format_subtitle(series)
        self.subtitle.setText(subtitle_text)

        self.score_label.setText(series['score'])
        self.series_eps_img = self.build_image(series['image'], self.FULL_IMG_SIZE, 
                                               self.series_eps_img)
        # Add related shows
        self.related_layout = self.reset_layout(self.related_layout, 
                                                self.scrollAreaWidgetContents_5)
        self.add_related_entries(series)

        # Add recommended entries
        self.rec_layout = self.reset_layout(self.rec_layout, 
                                            self.scrollAreaWidgetContents_6)
        self.add_rec_entries(series)
        
        # Populate episode list for series and map back to dictionary
        self.populate_ep_list(series['path'])
        self.ep_names = self.ep_name_map(series['path'])

        ### Set series_info page ###
        self.score_label_info.setText(series['score'])
        self.series_info_img = self.build_image(series['image'], self.FULL_IMG_SIZE, 
                                                self.series_info_img)
        info = sformat.format_series_info(series)
        self.series_info_text.setText(info)

        ### Set series_cast page ###
        # Only build if on the cast page
        if self.stackedWidget.currentIndex() == 2:
            self.add_cast_entries(series['id'])

    def load_char_listener(self, item):
        """
        An interface to load_char(), reacts to events that want to call it
        """
        # Prevent unintended calls
        if not item:
            return
        # Get name of person from widget and load
        self.load_char(item.text())

    def load_char(self, name):
        """
        Load information about a character given their name
        The name given can be either:
            - The shortened (display) version
            - The full version stored in the database
        """
        # Remember as most recently loaded char
        self.load_char_state = name
        # Try to get original name from dictionary
        try:
            name = self.char_names[name]
        except:
            pass
        # Retrieve character info from database
        char = db.char_from_name(name)
        
        # Set the char_info page
        self.stackedWidget.setCurrentIndex(3)
        self.button_right.hide()
        self.button_left.hide()
        
        # Set and save title, set subtitle and image
        self.title_label.setText(name)
        self.subtitle.setText("")
        self.char_img = self.build_image(char['image'], self.FULL_IMG_SIZE, self.char_img)

        # Set description for character
        info = sformat.format_char_info(char)
        self.char_desc.setText(info)

        # Set actor info for character
        actor = db.actor_from_char_id(char['id'])
        self.char_actor_img = self.build_icon(actor['image'], self.SMALL_IMG_SIZE, 
                                              self.char_actor_img)
        actor_name = sformat.format_actor_name(actor['name'])
        self.char_actor_name.setText(actor_name)

        # Load apperances in app_list
        #self.populate_list(self.app_list, char['appearances'])
        self.populate_list(self.app_list, db.get_appearances(char['id']))

    def load_person_listener(self, item):
        """
        An interface to load_person(), reacts to events that want to call it
        """
        # Prevent unintended calls
        if not item:
            return
        # Get name of person from widget and load
        self.load_person(item.text())

    def load_person(self, name):
        """
        Load information about a person from their name and builds the page
        """
        # Remember as most recently loaded person
        self.load_person_state = name

        # Retrieve person info from database
        person = db.person_from_name(name)
        
        # Set the person_info page
        self.stackedWidget.setCurrentIndex(4)
        self.button_right.hide()
        self.button_left.hide()
    
        # Set and save title, set subtitle and image
        self.title_label.setText(name)
        self.subtitle.setText("")
        self.person_img = self.build_image(person['image'], self.FULL_IMG_SIZE,
                                           self.person_img)
        # Set description for person
        info = sformat.format_char_info(person)
        self.person_desc.setText(info)

        # Reset role_layout
        self.role_layout = self.reset_layout(self.role_layout, 
                                             self.scrollAreaWidgetContents_4)
        # Load role and staff entries
        self.add_role_entries(name)
        self.add_staff_pos_entries(name)

    #-------------------------------------------------------------------------------
    # Miscellaneous Events
    #-------------------------------------------------------------------------------

    # When the play button is pressed, get the video file and play it
    def play_episode(self):
        # Make sure an item in the episode list is selected first
        if self.episode_list.currentItem():
            # From text on currently selected item, get file name from dictionary
            ep = self.episode_list.currentItem().text()
            file_name = self.ep_names[ep]
            # Play the video
            tools.play_video(self.series_path, file_name)

