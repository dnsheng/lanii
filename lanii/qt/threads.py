import sys
import time

import requests
from PyQt4 import QtCore, QtGui

import lanii.controller as controller

class QImportThread(QtCore.QThread):
    """
    This class is a general thread meant for importing.
        - refresh() signals for a refresh() call in the main window.
        - finished() signals that importing is done, and to hide the statusbar
        - hideGif() signals that the loading gif in the statusbar should be hidden
        - statusMessage() changes the message in the statusbar
    Obviously, a thread is needed to prevent the main window from hanging.
    Furthermore, the signals are needed to change the statusbar as widgets cannot
    be modified outside the main thread.
    """
    refresh = QtCore.pyqtSignal()
    finished = QtCore.pyqtSignal()
    hideGif = QtCore.pyqtSignal()
    statusMessage = QtCore.pyqtSignal(str, int)

    # Get the folder to import in the init, because run() can't accept args
    def __init__(self, info):
        QtCore.QThread.__init__(self)
        self.info = info
    
    # Print the error found when importing failed
    def error(self, e):
        self.statusMessage.emit("An error occurred during importing: " + str(e), 4000)
        self.hideGif.emit()
        time.sleep(4)
        self.finished.emit()

class QImportSeries(QImportThread):
    """
    This extends QImportThread and has a new function run() for importing a series
    """
    def __init__(self, info):
        QImportThread.__init__(self, info)

    def run(self):
        st = int(time.time())
        self.statusMessage.emit("Importing series: " + self.info , 0)
        #Try importing, catch and display errors
        controller.import_series(self.info)
        #try:
        #    controller.import_series(self.info)
        #except requests.exceptions.HTTPError as err:
        #    print(err)
        #    self.error(err)
        #    return
        #except:
        #    e = sys.exc_info()[0]
        #    print(e)
        #    self.error(e)
        #    return
        ## Display msg for 4 sec, refresh to show things, exit after message to finishes
        self.statusMessage.emit("Successfully imported series: " + self.info, 4000)
        et = int(time.time())
        tt = et - st
        print("Total time: " + str(tt))
        self.refresh.emit()
        time.sleep(4)
        self.finished.emit()

class QImportChar(QImportThread):
    """
    This extends QImportThread and has a new function run() for importing a character
    """
    def __init__(self, info):
        QImportThread.__init__(self, info)

    def run(self):
        self.statusMessage.emit("Importing character: " + self.info, 0)
        # Try importing, catch and display errors
        try:
            controller.import_character(self.info)
        except:
            e = sys.exc_info()[0]
            print(e)
            self.error(e)
            return
        self.statusMessage.emit("Successfully imported character: " + self.info, 4000)
        self.refresh.emit()
        time.sleep(4)
        self.finished.emit()

class QImportPerson(QImportThread):
    """
    This extends QImportThread and has a new function run() for importing a person
    """
    def __init__(self, info):
        QImportThread.__init__(self, info)

    def run(self):
        self.statusMessage.emit("Importing person: " + self.info, 0)
        # Try importing, catch and display errors
        try:
            controller.import_person(self.info)
        except:
            e = sys.exc_info()[0]
            print(e)
            self.error(e)
            return
        self.statusMessage.emit("Successfully imported person: " + self.info, 4000)
        self.refresh.emit()
        time.sleep(4)
        self.finished.emit()
