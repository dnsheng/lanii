from PyQt4 import QtCore, QtGui

class QEPushButton(QtGui.QPushButton):
    """
    Inherits from QPushButton and meant for any clickable images.
    The main purpose is to change the cursor when hovered over the image.
    """

    def __init__(self, parent=None):
        super(QEPushButton, self).__init__(parent)
        self.setMouseTracking(True)

    def enterEvent(self, event):
        self.setCursor(QtCore.Qt.PointingHandCursor)

    def leaveEvent(self, event):
        self.setCursor(QtCore.Qt.ArrowCursor)
