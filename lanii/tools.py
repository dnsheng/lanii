import os
import re
import threading

import lanii.constants as constants

def walklevel(path, depth = 1):
    """
    It works just like os.walk, but you can pass it a level parameter
    that indicates how deep the recursion will go.
    If depth is -1 (or less than 0), the full depth is walked.
    """
    # if depth is negative, just walk
    if depth < 0:
        for root, dirs, files in os.walk(path):
            yield root, dirs, files

    # path.count works because is a file has a "/" it will show up in the list
    # as a ":"
    path = path.rstrip(os.path.sep)
    num_sep = path.count(os.path.sep)
    for root, dirs, files in os.walk(path):
        yield root, dirs, files
        num_sep_this = root.count(os.path.sep)
        if num_sep + depth <= num_sep_this:
            del dirs[:]

def valid_files(path, depth):
    """
    Detect if a folder is valid. Validity is if within the given depth, files
    with extensions matching those in constants.FILE_TYPES exist
    """
    valid = []
    for root, dirs, files in walklevel(path, depth):
        for f in files:
            for ext in constants.FILE_TYPES:
                if f.endswith(ext):
                    valid.append(f)
    return valid

def play_video(path, video):
    """
    Given a path and a video files, the two are combined.
    Depending on the video extension, an appropriate player is chosen and the
    video is played in a seperate thread.
    """
    episode = os.path.join(path, video)
    ext = episode.rsplit('.', 1)[1]
    if ext == "sftp":
        cmd = "mftp \"" + episode + "\" &"
    else:
        cmd = "mpv \"" + episode + "\" &"
    play_thread = threading.Thread(target=os.system, args=(cmd,))
    play_thread.start()

def valid_folder(folder):
    """
    A folder is valid if it contains at least one file matching those in 
    constants.FILE_TYPES
    """
    result = False
    if os.path.isdir(folder):
        for f in os.listdir(folder):
            for ext in constants.FILE_TYPES:
                if f.endswith(ext):
                    result = True
    return result

def valid_episode(title):
    """
    Any episode that contains the following keywords are not displayed in lanii
    """
    keywords = ["NCED", "NCOP", "OP", "ED", "OVA", "SP1"]
    return not any(k in title for k in keywords)
