"""
A generic class that implements major functions to scrape information for series,
characters, and people. As main scraping functionality by the Scrape class is
performed by the module, any module written should follow the guidelines in the
abstract Module class. Doing so will ensure compatibility with this class.
"""
import copy
import requests
from multiprocessing.dummy import Pool as ThreadPool

import lanii.scrape.scrape_utils as su

class Scrape:
    """
    This is a generic scraper that handles the higher-level processes of web scraping.
    The module is added on init and handles the details of parsing the HTML for info.
    Scrape calls the visible functions from its module object in order to get that info
    and create dictionaries (Series, Character, Person).
    Functions within scrape should be generic enough that any module that implements the
    visible functions should work (i.e. scraping for any website).
    There is also a unique session that is used for all HTTP requests.
    """

    def __init__(self, module):
        """
        A pool of 5 threads is created for scraping. May require adjustments.
        A module object is created before the Scrape object and set on Scrape's init.
        A new session is also made per Scrape object.
        The gzip encoding is preferred for speed and less demand on the servers.
        """
        self.pool = ThreadPool(5)
        self.module = module
        self.session = requests.Session()
        self.session.headers = {"User-Agent": "lanii scraper/0.1",
                                "Accept-encoding": "gzip",}
        self.module.set_session(self.session)
        requests.packages.urllib3.disable_warnings()

    def _new_module(self, url):
        """
        Create a new module, loading in the URL and soup from the URL
        A new module is needed otherwise the multi-processing is wonky
        """
        module = copy.copy(self.module)
        url = module.clean_url(url)
        module.set_url(url)
        soup = su.soup_request(module.session, url)
        module.set_soup(soup)

        return module

    def scrape_series(self, url, src):
        """
        Scrape for information on a series with the object's module.
        The URL is for the series and the src is the absolute path that the video files
        reside in.
        """
        # Initialize the module
        module = self._new_module(url)

        # Scrape for information with module
        s_id        = module.scrape_series_id()
        s_image     = module.scrape_series_image()
        s_name      = module.scrape_series_name()
        s_synopsis  = module.scrape_series_synopsis()
        s_format    = module.scrape_series_format()
        s_episodes  = module.scrape_series_episodes()
        s_status    = module.scrape_series_status()
        s_aired     = module.scrape_series_aired()
        s_premier   = module.scrape_series_premiered()
        s_broadcast = module.scrape_series_broadcast()
        s_producers = module.scrape_series_producers()
        s_licensors = module.scrape_series_licensors()
        s_studios   = module.scrape_series_studios()
        s_source    = module.scrape_series_source()
        s_genres    = module.scrape_series_genres()
        s_duration  = module.scrape_series_duration()
        s_rating    = module.scrape_series_rating()
        s_score     = module.scrape_series_score()
        s_sequels   = module.scrape_series_sequels()
        s_prequels  = module.scrape_series_prequels()
        s_alt       = module.scrape_series_alternatives()
        s_side      = module.scrape_series_sides()
        s_other     = module.scrape_series_others()
        s_recs      = self._scrape_recs(url)
        s_path      = src
        
        # Create series dictionary with info and return
        return {'id': s_id, 'image': s_image, 'name': s_name, 'synopsis': s_synopsis,
                'format': s_format, 'episodes': s_episodes, 'status': s_status,
                'aired': s_aired, 'premier': s_premier, 'broadcast': s_broadcast,
                'producers': s_producers, 'licensors': s_licensors, 'studios': s_studios,
                'source': s_source, 'genres': s_genres, 'duration': s_duration,
                'rating': s_rating, 'score': s_score, 'path': s_path,
                'prequels': s_prequels, 'alternates': s_alt, 'sides': s_side,
                'sequels': s_sequels, 'others': s_other, 'recs': s_recs}

    def _scrape_recs(self, url):
        """
        Multi-process scraping of recommended series
            - Image and ID scraping is multi-processed
            - Title and description are sequential (more efficient this way)
        """
        # Initialize the module
        module = self._new_module(url)
        # Get the rec URLs
        rec_urls = module.scrape_rec_urls()
        # Scrape recs
        recs = self.pool.map(module.scrape_series_rec, rec_urls)
        
        # Combine and organize all recs to a list and return
        final_recs = []
        for r in recs:
            r_url = r[0]
            id_ = r[1]
            img = r[2]
            title = rec_urls[r_url][0]
            desc = rec_urls[r_url][1]
            final_recs.append((id_, img, title, desc))

        return final_recs

    def scrape_char_nourl(self, char_name, char_id):
        """
        Build URL for character with module then scrape as normal
        """
        char_url = self.module.make_char_url(char_name, char_id)
        return self.scrape_character(char_url)
    
    def scrape_character(self, url):
        """
        Scrape for information on a character with the object's module.
        The URL is for the character's page
        The character object is returned, as well as a soup of the character's page
        """
        # Initialize the module
        module = self._new_module(url)
        
        # Scrape for information with the module
        # NOTE: For multi-process, explicit passing of URL is better
        #           - If _scrape_init() is relied on, then the ID is wonky
        #       Thus, re-think the function calls with Scrape and Module
        c_id    = module.scrape_char_id(url)
        c_name  = module.scrape_char_name()
        c_desc  = module.scrape_char_desc()
        c_img   = module.scrape_char_image(url)
        
        # Build a character dict and return
        return {'id': c_id, 'image': c_img, 'name': c_name, 'desc': c_desc}

    def _scrape_character_cast(self, url):
        """
        This is a special scrape_character() function for _scrape_cast()
        The url needs to be included with the character object to easily refer back
        """
        character = self.scrape_character(url)
        return [character, url]

    def scrape_person_nourl(self, name, id_):
        """
        Build URL for person with module then scrape as normal
        """
        person_url = self.module.make_person_url(name, id_)
        return self.scrape_person(person_url)

    def scrape_person(self, url):
        """
        Scrape for information on a person with the object's module
        The URL is for the person's page
        The person object is returned
        """
        # Check for URL
        if not url:
            return None

        # Initialize the module
        module = self._new_module(url)
        
        # Scrape for information with the module
        p_id    = module.scrape_person_id(url)
        p_name  = module.scrape_person_name()
        p_desc  = module.scrape_person_desc()
        p_img   = module.scrape_person_image(url)

        # Build a person dict and return
        return {'id': p_id, 'image': p_img, 'name': p_name, 'desc': p_desc}

    def _scrape_person_cast(self, url):
        """
        This is a special scrape_person() function for _scrape_cast()
        The url is used to link the person object with the original url.
        This is necessary because we want to connec the character to the actor.
        """
        person = self.scrape_person(url)
        return [person, url]

    def _create_role_urls(self, role_dict):
        """
	From role_dict, return a list of character URLs and actor URLs
	The list is used for pool.map()
	"""
        char_urls = []
        actor_urls = []
        for item in role_dict:
            char_urls.append(item)
            actor_urls.append(role_dict[item]['actor_url'])
        return (char_urls, actor_urls)
	
    def _link_char_actor(self, role_dict, char_pool, actor_pool, actor_urls):
        """
        Return a list of characters that contain their role and id of their actor
        """
        char_list = []
        for index, item in enumerate(char_pool):
            character = item[0]
            char_url = item[1]
            # Add character role
            character['role'] = role_dict[char_url]['role']
            # Order of actors in actor_urls matches with order of chars in char_pool
            actor_url = actor_urls[index]
            # No actor for the character, use dummy actor
            if not actor_url:
                character['person_id'] = 0
            else:
                # Get compare actor_url to urls in actor_pool to get the Actor dict
                for actor in actor_pool:
                    if actor_url == actor[1]:
                        # Link the actor's id to the character
                        character['person_id'] = actor[0]['id']
                        break
            # Add modified character to char_list
            char_list.append(character)
        return char_list

    def _get_actor_list(self, actor_pool):
        """
        Seperate out actors from actor_pool and return as a list
        """
        actor_list = []
        for actor in actor_pool:
            if actor[0]:
                actor_list.append(actor[0])
        return actor_list

    def _link_staff_role(self, staff_pool, staff_dict):
        """
        Return a list of staff that contain their roles
        """
        staff_list = []
        for item in staff_pool:
            staff = item[0]
            staff_url = item[1]
            staff['role'] = staff_dict[staff_url]
            staff_list.append(staff)
        return staff_list

    def _scrape_cast(self, url):
        """
        The cast of a series given by the URL is scraped.
        The cast includes the characters and their actors, as well as staff members.
		Returns a list containing 3 other lists:
            1. char_list is a list of Character dicts
            2. actor_list is a list of Person dicts
            3. staff_list is a list of Person dicts
        """
        # Initialize the module
        module = self._new_module(url)

        # Retrieve all URLs for roles and staff
        cast_list = module.get_cast_list()
        role_dict = cast_list[0]
        staff_dict = cast_list[1]

        # Create a list of char and actor URLs
        role_urls = self._create_role_urls(role_dict)
        char_urls = role_urls[0]
        actor_urls = role_urls[1]

        # Scrape from list of URLs to list of dicts
        char_pool = self.pool.map(self._scrape_character_cast, char_urls)
        actor_pool = self.pool.map(self._scrape_person_cast, actor_urls)
        staff_pool = self.pool.map(self._scrape_person_cast, staff_dict)

	# Create final lists for characters, actors, and staff
        char_list = self._link_char_actor(role_dict, char_pool, actor_pool, actor_urls)
        actor_list = self._get_actor_list(actor_pool)
        staff_list = self._link_staff_role(staff_pool, staff_dict)

        return [char_list, actor_list, staff_list]

    def scrape_all(self, request, src):
        """
        For a series request, scrapes the series information as well as all characters
        and people (actors/staff) involved.
        A Scraps dict is returned
        """
        # Turn request into Google url
        url = su.init_query(request)
        # Append to Google url for module
        url = self.module.format_query(url)
        # Create soup from Google search
        g_soup = su.soup_request(self.module.session, url)
        # Get url for module specific site
        url = self.module.get_module_url(g_soup)
        
        # Scrape the series
        series = self.scrape_series(url, src)

        # Get all roles and staff
        output = self._scrape_cast(url)
		
        return {'series': series, 'characters': output[0], 'actors': output[1],
	        'staff': output[2]}
