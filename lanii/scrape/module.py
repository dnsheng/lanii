import abc

class Module(abc.ABC):
    """
    An abstract class that handles the implementation details of website scraping.
    Ideally, one module is created for each website/source of information.
    The bare minimum is implementation of the abstract methods below which act as the API.
    A module object is created and used as a parameter to the Scrape object instantiation.
    Scrape relies on these functions to retrieve information.
    """
    def __init__(self):
        self.session = None
        self.soup = None
        self.url = None

    def set_session(self, session):
        """
        A session should be owned by the module and passed whenever soup_request() is called.
        This ensures that the scraping is done through a single request.
        """
        self.session = session

    def set_soup(self, soup):
        """
        Setting a local soup prevents the need to constantly pass the soup when calling
        functions in the module.
        """
        self.soup = soup

    def set_url(self, url):
        """
        A url is owned by the module and is used for soup_request() calls
        """
        self.url = url

    @abc.abstractmethod
    def format_query(self, url):
        """
        This will format the Google query for the module's site
        """
        pass

    ### Scraping for series ###

    @abc.abstractmethod
    def scrape_series_id(self):
        """
        Return the series id
        """
        pass

    @abc.abstractmethod
    def scrape_series_image(self):
        """
        Download the poster image for the series and return the file's path
        """
        pass
 
    @abc.abstractmethod
    def scrape_series_name(self):
        """
        Return the series name
        """
        pass
    
    @abc.abstractmethod
    def scrape_series_synopsis(self):
        """
        Return the synopsis of the series
        """
        pass
    
    @abc.abstractmethod
    def scrape_series_format(self):
        """
        Return the format of the series (TV, Movie, OVA, etc.)
        """
        pass

    @abc.abstractmethod
    def scrape_series_episodes(self):
        """
        Return the # of episodes in the series
        """
        pass

    @abc.abstractmethod
    def scrape_series_status(self):
        """
        Return the status of the series (Finished, Currently airing, etc.)
        """
        pass
    
    @abc.abstractmethod
    def scrape_series_aired(self):
        """
        Return when the series airs/aired
        """
        pass

    @abc.abstractmethod
    def scrape_series_premiered(self):
        """
        Return when the series premiers/premiered
        """
        pass
    
    @abc.abstractmethod
    def scrape_series_broadcast(self):
        """
        Return when the series broadcasts/was broadcasted
        """
        pass
    
    @abc.abstractmethod
    def scrape_series_source(self):
        """
        Return the source of the series (Novel, Original, etc.)
        """
        pass

    @abc.abstractmethod
    def scrape_series_producers(self):
        """
        Return a dictionary of producers, [ID] mapped to name
        """
        pass
    
    @abc.abstractmethod
    def scrape_series_licensors(self):
        """
        Return a dictionary of licensors, [ID] mapped to name
        """
        pass
    
    @abc.abstractmethod
    def scrape_series_studios(self):
        """
        Return a dictionary of studios, [ID] mapped to name
        """
        pass
    
    @abc.abstractmethod
    def scrape_series_genres(self):
        """
        Return a dictionary of genres, [ID] mapped to name
        """
        pass
    
    @abc.abstractmethod
    def scrape_series_duration(self):
        """
        Return the runtime duration of the series
        """
        pass
    
    @abc.abstractmethod
    def scrape_series_rating(self):
        """
        Return the rating/certification of the series (PG, R, etc.)
        """
        pass
    
    @abc.abstractmethod
    def scrape_series_score(self):
        """
        Return the viewer/critic score of the series
        """
        pass
    
    @abc.abstractmethod
    def scrape_series_prequels(self):
        """
        Return a list of prequel series in a list of (ID, img, name)
        """
        pass
    
    @abc.abstractmethod
    def scrape_series_alternatives(self):
        """
        Return a list of alternative series in a list of (ID, img, name)
        """

        pass
    
    @abc.abstractmethod
    def scrape_series_sides(self):
        """
        Return a list of side story series in a list of (ID, img, name)
        """
        pass
    
    @abc.abstractmethod
    def scrape_series_sequels(self):
        """
        Return a list of sequel series in a list of (ID, img, name)
        """

        pass
    
    @abc.abstractmethod
    def scrape_series_others(self):
        """
        Return a list of other series in a list of (ID, img, name)
        """

        pass

    @abc.abstractmethod
    def scrape_rec_urls(self):
        """
        Return a dict of URLS for recommended show URLs -> [description, title]
        The description is written by a person detailing why the two are related.
        It can easily also be the description of the recommended series or an empty string.
        """
        pass
    
    @abc.abstractmethod
    def scrape_series_rec(self, rec_url):
        """
        Return a recommended series (ID, img) for the given URL
        Combine with info from scrape_rec_urls() (aka the title & desc) in Scraper.py
        """
        pass

    ### Scraping for characters ###

    @abc.abstractmethod
    def scrape_char_id(self):
        """
        Return the character's id
        """
        pass

    @abc.abstractmethod
    def scrape_char_image(self):
        """
        Download the character's image and return the file path
        """
        pass

    @abc.abstractmethod
    def scrape_char_name(self):
        """
        Return the character's name
        """
        pass
    
    @abc.abstractmethod
    def scrape_char_desc(self):
        """
        Return the character's description
        """
        pass
    
    ### Scraping for people ###

    @abc.abstractmethod
    def scrape_person_id(self):
        """
        Return the person's id
        """
        pass
    
    @abc.abstractmethod
    def scrape_person_image(self):
        """
        Download the person's image and return the file path
        """
        pass
    
    @abc.abstractmethod
    def scrape_person_desc(self):
        """
        Return the person's description/biography
        """
        pass
    
    ### Scraping for all ###

    @abc.abstractmethod
    def get_cast_list(self):
        """
        For the series, return a list that comprises of two other lists:
            1. Roles - A list of Role dicts
            2. Staff - A list of Staff dicts
        """
        pass
