"""
These are functions that are meant to help scrape data from search queries
"""

import re
from time import sleep

from urllib.parse import quote
from bs4 import BeautifulSoup

def init_query(query):
    """
    The initial query from the title is formatted:
        - Special characters are substituted to explicit keywords
        - Unicode is converted
        - Spaces become %20
    Then it becomes a Google search URL
    """
    # Two exclamation marks means season 2 (K-On!! but not Keijo!!!!!!!!)
    sub_query = re.sub(r'([^\!]+)\!\!$', '\\1 S2', query)
    print(sub_query)
    # Two question marks means season 2 (Gochuu S2)
    sub_query = re.sub(r'\?\?', ' S2', sub_query)
    # Deal with unicode
    sub_query = quote(sub_query)
    # Replace spaces with %20
    sub_query = re.sub(r' ', '%20', sub_query)
    final_query = "https://www.google.com/search?q=" + sub_query
    return final_query

def soup_request(session, url):
    """
    A given URL is requested in a given session
    The response is parsed by BeautifulSoup as HTML and the soup is returned
    requests automatically decompresses the gzip response
    """
    sleep(0.2)
    request = session.get(url, verify=False)
    request.raise_for_status()
    content = request.content
    return BeautifulSoup(content, 'html.parser')
