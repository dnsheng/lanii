import os
import re
import shutil

from urllib.parse import quote

import lanii.constants as constants
import lanii.scrape.scrape_utils as su

from lanii.scrape.module import Module

class MALModule(Module):
    """
    This module specializes in scraping information from MyAnimeList
    """
    def __init__(self):
        super().__init__()
        self.tags = None
        self.relatives = None
    
    def set_soup(self, soup):
        self.soup = soup
        # Contains all the tags in MAL, used for parsing
        self.tags = self.soup.find_all("span", {"class": "dark_text"})
        # The HTML for all related series
        self.relatives = self.soup.find_all("td", {"class": "ar fw-n borderClass"})

    def format_query(self, url):
        """
        Format the Google URL for MAL
        """
        return url + "%20MAL"

    def clean_url(self, url):
        """
        Clean a URL that may contain unicode characters so urllib can read it
        """
        split = url.rsplit('/', 1)
        url_start = split[0]
        url_end = split[1]
        new_end = quote(url_end)
        new_url = url_start + "/" + new_end
        return new_url

    def get_module_url(self, soup):
        """
        From the Google search soup find and return the MAL URL
        """
        for link in soup.find_all('a'):
            if "myanimelist.net/anime" in link.get('href'):
                return self._format_url(link.get('href'))

    ### General hidden functions ###
    
    def _format_url(self, url):
        """
        Format the found MAL URL from the Google search and return it
        If not found, return None
        """
        if url:
            module_url = url[7:]
            module_url = re.sub(r'&sa.*', '', module_url)
            module_url = re.sub(r'/characters$', '', module_url)
            return module_url
        else:
            return None

    def _get_id(self, url):
        """
        Cut the ID from a MAL URL
        """
        return url.rsplit('/', 2)[1]

    def _get_img_url(self):
        return self.soup.find(itemprop="image")["src"]

    def _get_img(self, id_, url, dest):
        """
        Downloads the image from the url to the dest folder
        If no image exists, copy over the placeholder asset
        The image is named after the id
        """
        img_file = id_ + ".jpg"
        img_dest = os.path.join(dest, img_file)
        if url != "https://myanimelist.cdn-dena.com/img/sp/icon/apple-touch-icon-256.png":
            img = self.session.get(url, stream=True, verify=False)
            with open(img_dest, 'wb') as f:
                for chunk in img.iter_content(chunk_size=1024):
                    f.write(chunk)
        else:
            # Shutil doesn't allow overwrite, so only copy if it's a new entry
            if not os.path.isfile(img_dest):
                # Copy image asset to a new file
                tmp_dest = os.path.join(constants.IMAGE_ASSETS, img_file)
                shutil.copy(constants.NO_IMG_ASSET, tmp_dest)
                # Move new file over to the data folder
                shutil.move(tmp_dest, dest)
        return img_dest

    def _syn_format(self, syn):
        """
        Remove the credits/source
        """
        syn = re.sub(r'\[Written by.*', '', syn)
        syn = re.sub(r'\[Source.*', '', syn)
        syn = re.sub(r'\(Source.*', '', syn)
        return syn

    def _index_for_tag(self, field):
        """
        The index for a field within the MAL tags is returned
        """
        for index, item in enumerate(self.tags):
            if item.string == field:
                return index

    def _getstr(self, span):
        """
        The string from a MAL span retrieved from tags is returned
        """
        return span.next_sibling.strip()

    def _parse_span(self, span):
        """
        A dictionary of items for MAL is returned, where [ID] maps to "Name"
        It is used for: Producers, Licensors, Studios, and Genres
        """
        items = {}
        for item in span.next_siblings:
            if item.name == "a":
                item_id = self._get_id(item.get('href'))
                item_name = item.string
                if not (item_name == "None found" or item_name == "add some"):
                    items[item_id] = item_name
        return items

    def _get_related(self, span, code):
        """
        A list of all related series for a series is returned
        These include: Prequels, Sequels, Alternatives, Side Stories, Other, and Adaptation
        The type to look for and return is based on the 'code' parameter and its mapping to
        the 'fields' dictionary in this function.
        The return format is a list of: (ID, img, name/title)
        """
        fields = {'P': "Prequel:",
                  'S': "Sequel:",
                  'A': "Alternative version:",
                  'Y': "Side story:",
                  'O': "Other:",
                  'X': "Adaptation:"}
        # Init list to append/return
        related = []
        
        for item in span:
            # Only append items for selected field
            if item.string.strip() == fields[code]:
                siblings = item.find_next_siblings("td", {"class": "borderClass"})
                for s in siblings:
                    relatives = s.find_all("a")
                    for r in relatives:
                        r_id = self._get_id(r.get('href'))
                        r_page = "https://myanimelist.net" + r.get('href')
                        r_page = self.clean_url(r_page)
                        r_soup = su.soup_request(self.session, r_page)
                        r_img_url = r_soup.find(itemprop="image")["src"]
                        r_img_url = self.clean_url(r_img_url)
                        r_img = self._get_img(r_id, r_img_url, constants.SERIES_IMG_PATH)
                        related.append((r_id, r_img, r.string))
        return related

    def _is_character(self, item):
        """
        Determine if the item is for a character field in MAL
        """
        text = item.small.string.strip()
        if text == "Supporting" or text == "Main":
            return True
        else:
            return False

    def _clean_html_tags(self, string):
        """
        Remove HTML tags from a string (mainly br, div, span, and input)
        """
        string = re.sub(r'<[/]?b[r]?[/]?>', '', string)
        string = re.sub(r'<[/]?div.*', '', string)
        string = re.sub(r'<[/]?span.*', '', string)
        string = re.sub(r'<input.*', '', string)
        return string

    def _desc_format(self, soup):
        """
        The description for a character on MAL is formatted
        There are a few cases:
            - Bold spans are used in description
            - No bold spans used in description
            - No description
            - No voice actor, yet a character description
        """
        text = soup.find_all("div", {"class": "normal_header"})[2]
        tmp_desc = ""
        if text.next_sibling.name == 'b':
            # Bold formatting in desc
            # Construct general desc first
            l = list(text.next_siblings)
            tmp_desc += l[0].string + l[1] + "\n"
            body = l[2].prettify()
            main = body.split('<div class="normal_header"')[0]
            # Clean it up, removing HTML tags
            main = self._clean_html_tags(main)
            # Remove source tag
            main = self._syn_format(main)
            # Remove empty lines and leading spaces
            tmp_list = main.split('\n')
            for t in tmp_list:
                if len(t.strip()) > 0:
                    if t.strip()[-1:] == ":":
                        tmp_desc += t.strip()
                    elif t.strip()[0] == ":":
                        # Ex. Rin Kaga has colon char on different line
                        tmp_desc = tmp_desc[:-1] + t.strip() + "\n"
                    else:
                        tmp_desc += " " + t.strip() + "\n"
            # Remove leading spaces and the last newline
            tmp_desc = re.sub(r'\n\ ', '\n', tmp_desc)
            # Remove spoiler warning
            tmp_desc = re.sub(r'Manga spoilers.*', '', tmp_desc)
            tmp_desc = tmp_desc.strip()
            desc = tmp_desc
        else:
            backup_text = text
            # No special bold formatting - average description
            text = text.next_sibling.next_sibling
            for t in text:
                if t.string:
                    tmp_desc += t.string.strip() + "\n"
            # Sometimes the desc has tags, so remove
            tmp_desc = self._syn_format(tmp_desc)
            # Remove the fragment that trails at the end
            tmp_desc = re.sub(r'Voice Actors', '', tmp_desc)
            desc = tmp_desc.rstrip('\n')
            # Edge cases
            if "No voice actors" in tmp_desc:
                desc = backup_text.next_sibling.string
            elif not desc:
                desc = backup_text.next_sibling.string
                if not desc:
                    desc = "No biography written."
        return desc

    ### Scraping for series ###

    def scrape_series_id(self):
        return self._get_id(self.url)

    def scrape_series_image(self):
        id_ = self.scrape_series_id()
        img_url = self._get_img_url()
        return self._get_img(id_, img_url, constants.SERIES_IMG_PATH)

    def scrape_series_name(self):
        return self.soup.find(itemprop="name").get_text()
    
    def scrape_series_synopsis(self):
        syn = self.soup.find(itemprop="description").get_text()
        return self._syn_format(syn)
    
    def scrape_series_format(self):
        tag_index = self._index_for_tag("Type:")
        if tag_index:
            return self.tags[tag_index].next_sibling.next_sibling.string
        else:
            return None

    def scrape_series_episodes(self):
        tag_index = self._index_for_tag("Episodes:")
        if tag_index:
            return self._getstr(self.tags[tag_index])
        else:
            return None

    def scrape_series_status(self):
        tag_index = self._index_for_tag("Status:")
        if tag_index:
            return self._getstr(self.tags[tag_index])
        else:
            return None
    
    def scrape_series_aired(self):
        tag_index = self._index_for_tag("Aired:")
        if tag_index:
            return self._getstr(self.tags[tag_index])
        else:
            return None
    
    def scrape_series_premiered(self):
        tag_index = self._index_for_tag("Premiered:")
        if tag_index:
            return self.tags[tag_index].next_sibling.next_sibling.string
        else:
            return None
    
    def scrape_series_broadcast(self):
        tag_index = self._index_for_tag("Broadcast:")
        if tag_index:
            return self._getstr(self.tags[tag_index])
        else:
            return None

    def scrape_series_source(self):
        tag_index = self._index_for_tag("Source:")
        if tag_index:
            return self._getstr(self.tags[tag_index])
        else:
            return None
    
    def scrape_series_duration(self):
        tag_index = self._index_for_tag("Duration:")
        if tag_index:
            return self._getstr(self.tags[tag_index])
        else:
            return None
    
    def scrape_series_rating(self):
        tag_index = self._index_for_tag("Rating:")
        if tag_index:
            return self._getstr(self.tags[tag_index])
        else:
            return None
    
    def scrape_series_score(self):
        return self.soup.find(itemprop="ratingValue").get_text()
    
    def scrape_series_producers(self):
        tag_index = self._index_for_tag("Producers:")
        if tag_index:
            return self._parse_span(self.tags[tag_index])
        else:
            return None

    def scrape_series_licensors(self):
        tag_index = self._index_for_tag("Licensors:")
        if tag_index:
            return self._parse_span(self.tags[tag_index])
        else:
            return None
    
    def scrape_series_studios(self):
        tag_index = self._index_for_tag("Studios:")
        if tag_index:
            return self._parse_span(self.tags[tag_index])
        else:
            return None
    
    def scrape_series_genres(self):
        tag_index = self._index_for_tag("Genres:")
        if tag_index:
            return self._parse_span(self.tags[tag_index])
        else:
            return None

    def scrape_series_prequels(self):
        return self._get_related(self.relatives, 'P')
    
    def scrape_series_alternatives(self):
        return self._get_related(self.relatives, 'A')
    
    def scrape_series_sides(self):
        return self._get_related(self.relatives, 'Y')
    
    def scrape_series_sequels(self):
        return self._get_related(self.relatives, 'S')
    
    def scrape_series_others(self):
        return self._get_related(self.relatives, 'O')
    
    def scrape_rec_urls(self):
        """
        Given the URL for a series
        A dict of recommended show URLs -> [description, title] is returned

        WARNING: This is under the assumption that self.url is the URL of the series
        """
        recommendations = {}

        rec_url = self.url + "/userrecs"
        soup = su.soup_request(self.session, rec_url)

        for item in soup.find_all("strong"):
            if not re.match('^[0-9]+$', item.string):
                rec_title = item.string
                rec_url = self.clean_url(item.parent.get('href'))
                div = item.parent.parent.parent.find("div",
                                                     {"class": "detail-user-recs-text"})
                if not div.string:
                    rec_desc = re.split(' read more$', div.get_text())[0]
                else:
                    rec_desc = div.string
                recommendations[rec_url] = [rec_title, rec_desc]

        return recommendations
    
    def scrape_series_rec(self, rec_url):
        """
        Given the URL for a recommended series
        Return the ID and image
        Return the recommended series id, img, title, and desc
        """
        rec_id = self._get_id(rec_url)
        # Get the image from the series page
        rec_soup = su.soup_request(self.session, rec_url)
        rec_img_url = rec_soup.find(itemprop="image")["src"]
        rec_img_url = self.clean_url(rec_img_url)
        rec_img = self._get_img(rec_id, rec_img_url, constants.SERIES_IMG_PATH)

        return [rec_url, rec_id, rec_img]

    def _actor_desc(self, soup):
        """
        Format the description for a person on MAL
        """
        desc = ""
        for item in soup.findAll("span", {"class": "dark_text"}):
            if item.string != "More:" and item.next_sibling.string.strip():
                desc += item.string + item.next_sibling.string + "\n"
            elif item.string == "More:":
                # The spelling mistake is found on MAL
                text = soup.find("div", {"class": "people-informantion-more"}).get_text()
                desc += text
        # Sometimes the desc has tags, so remove
        desc = self._syn_format(desc)
        # Remove double empty lines
        desc = re.sub(r'\n\n', '\n', desc)
        desc = desc.strip()

        return desc

    ### Scraping for characters ###

    def scrape_char_id(self, url):
        return self._get_id(url)

    def scrape_char_image(self, url):
        id_ = self._get_id(url)
        img_url = self.soup.find(property="og:image")["content"]
        return self._get_img(id_, img_url, constants.CHAR_IMG_PATH)

    def scrape_char_name(self):
        return self.soup.find("h1", {"class": "h1"}).string
    
    def scrape_char_desc(self):
        return self._syn_format(self._desc_format(self.soup))
    
    ### Scraping for people ###

    def scrape_person_id(self, url):
        return self._get_id(url)
    
    def scrape_person_image(self, url):
        id_ = self._get_id(url)
        img_url = self.soup.find(property="og:image")["content"]
        return self._get_img(id_, img_url, constants.PERSON_IMG_PATH)

    def scrape_person_name(self):
        return self.soup.find("h1", {"class": "h1"}).string
    
    def scrape_person_desc(self):
        return self._actor_desc(self.soup)

    ### Scraping for all ###

    def get_cast_list(self):
        """
            Return a list called "output" comprised of two items:
                1. Roles - A dictionary of char_url -> role dict {char_url, actor_url, role}
                2. Staff - A dictionary of staff_url -> staff role

            WARNING: This is under the assumption that self.url is the url of the series
        """
        cast_list = []
        roles = {}
        staff = {}

        # Get url of all chars, actors, and staff and turn into soup
        url = self.clean_url(self.url + "/characters")
        soup = su.soup_request(self.session, url)

        # Parse everything
        for item in soup.findAll("div", {"class": "spaceit_pad"}):
            # Ensure that item is for charactors/actors and staff
            if item.small:
                if self._is_character(item):
                    # Get character
                    char_element = item.previous_sibling.previous_sibling
                    char_url = char_element.get('href')
                    char_role = item.small.string.strip()
                    char_soup = su.soup_request(self.session, char_url)
                    # Get Japanese VA from the char_soup
                    va_url = None
                    for i in char_soup.findAll("div", {"class": "normal_header"}):
                        if i.string == "Voice Actors":
                            for j in i.next_siblings:
                                if j.name == "table":
                                    if j.small.string == "Japanese":
                                        va_url = j.a.get('href')
                                        # Create new Role object and add to dictionary
                                        new_role = {'char_url': char_url,
                                                    'actor_url': va_url,
                                                    'role': char_role}
                    roles[char_url] = new_role
                else:
                    # Get staff
                    staff_element = item.previous_sibling.previous_sibling
                    staff_url = staff_element.get('href')
                    staff_role = item.small.string.strip()
                    staff[staff_url] = staff_role
        
        # Put all roles and staff into final list and return
        cast_list.append(roles)
        cast_list.append(staff)

        return cast_list

def make_char_url(self, name, id_):
    """
    The MAL URL for a character is constructed after given their name and id
    Used by the controller when importing
    """
    url = "https://myanimelist.net/character/" + str(id_) + "/"
    # Replace spaces with underscores
    name_part = re.sub(r'\s+', '_', name)
    url += name_part
    return url

def make_person_url(self, name, id_):
    """
    The MAL URL for a person is constructed after given their name and id
    Used by the controller when importing
    """
    url = "https://myanimelist.net/people/" + str(id_) + "/"
    # Replace spaces with underscores
    name_part = re.sub(r'\s+', '_', name)
    url += name_part
    return url
