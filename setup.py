#from distutils.core import setup
from setuptools import setup, find_packages

import lanii.constants as constants

setup(
    # Application name:
    name="lanii",
    # Version number
    version=constants.VERSION,
    description="Anime information scraper and displayer.",
    # Packages
    packages=find_packages(),
    package_data={
        'lanii': [
            'assets/images/*', 
            'assets/qt/*'
        ]
    },

    # Application author details:
    author="Dan Sheng",
    author_email="dnsheng@gmail.com",
    url="https://gitlab.com/dnsheng/lanii",

    # Include additional files into the package
    include_package_data=True,

    license="LICENSE.txt",
    long_description=open("README.md").read(),

    # Dependent packages (distributions)
    install_requires=[
        "Pillow",
        "SIP",
        "urllib3",
        "requests",
        "beautifulsoup4",
    ],

    entry_points={
        'gui_scripts': [
            'lanii=lanii.entry:lanii_qt',
        ],
    },
)
